%https://mp.weixin.qq.com/s/wpGI9uAi3qmMNydhevR83w
clear;clc;
%Nair算法
%公众号:好玩的MATLAB
X=[480.9,522,468.8,469.5,573.8,737.8,869.8,933.7,977.2,...
    997.7,1120.3,1176.1,1284.8,1422.1,1462.1,1499.7,...
    1473.1,1539.2,1637,1771,1886.5,1994.6,2145.7,2292,...
    2396.8,2387,2484.4,2580.8,2750.2,2915.7,3163.8,3231.9,...
    3319.5,3319.6,3484.,3550.6,3613.9,3833.1,4471.2,5283,...
    5803.2,6415.5,6797.9,7033.5,7636.3,8209.8,8979.1];
n=length(X)-1;
for t=1:n
    Z(t)=1/X(t)-1/X(t+1);
    S(t)=1/X(t)+1/X(t+1);
end
X1=[ones(46,1) S(1:n)'];
Y=Z';
[B,Bint,r,rint,stats]=regress(Y,X1);%最小二乘（OLS）
gamma=B(1,1);
beta=B(2,1);
b=log((1-beta)/(1+beta));
c=gamma*(1+exp(b))/(2*(exp(b)-1));
a=exp((sum(log(1./X(1:n)-c))-n*(n+1)*b/2)/n);
XX=1965:2016;
YY=1./(c+a*exp(b*([XX-1965])));
plot(XX,YY,'r-o')
hold on
plot(XX(1:length(X)),X,'g-^')
legend('预测值','实际值')
xlabel('年份');ylabel('二氧化碳排放量');
title('二氧化碳预测值和实际值曲线图(Nair法)')
set(gca,'XTick',[1965:4:2017])
grid on
format short;
forecast=YY(end-4:end);%CO2排放量的预测结果
MAPE=sum(abs(YY(1:n+1)-X)./X)/length(X);%平均相对差值
a,b,c
