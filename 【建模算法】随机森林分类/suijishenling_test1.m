clc;clear;close all;
load('iri_data.mat')
%%
data=iri_data;
num_train = ceil(0.8*size(data,1));%共500个样本
%构造随机选择序列
choose = randperm(length(data));
%构建训练数据
train_data = data(choose(1:num_train),1:end);
test_data = data(choose(num_train+1:end),1:end);
%最后一列为分类标签
label_train = train_data(:,end);
label_test = test_data(:,end);
% 1. 随机产生训练集/测试集
P_train = train_data;
P_test = test_data;
T_train = label_train;
T_test =label_test ;
ntree = 300;
%% III. 创建随机森林分类器
Model = TreeBagger(ntree,train_data(:,1:end-1),label_train,'Method','classification','OOBPredictorImportance','on');
%% IV. 仿真测试
predict_label= predict(Model, test_data(:,1:end-1));
num=0;
for i=1:length(T_test)
    a=predict_label{i,1};
    if str2double(a)==T_test(i,1)
        num=num+1;
    end
end
accuracy=num/length(T_test);
%%
imp = Model.OOBPermutedPredictorDeltaError;
figure;
bar(imp);
title('Curvature Test');
ylabel('Predictor importance estimates');
xlabel('Predictors');
h = gca;