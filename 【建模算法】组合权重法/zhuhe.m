%https://mp.weixin.qq.com/s/dgT7PSFT9u-7dSDxdEEJKQ
clc;clear;
data=xlsread('机器人方案.xlsx');
data1=data;
index=[1,3];    %第一三个指标为负向指标，
for i=1:length(index)
  data1(:,index(i))=(max(data(:,index(i)))-data(:,index(i)))/(max(data(:,index(i)))-min(data(:,index(i))));
end
index_all=1:size(data1,2); 
index_all(index)=[];    % 除负向指标外其余所有指标%%正向指标准化处理
index=index_all;
for i=1:length(index)
  data1(:,index(i))=(data(:,index(i))-min(data(:,index(i))))/(max(data(:,index(i)))-min(data(:,index(i))));
end
%一致性检验
W1=[0.3,0.4,0.15,0.15]';%权重1
W2=[0.4,0.3,0.15,0.15]';%权重2
W3=[0.25,0.25,0.25,0.25]';%权重3
W4=[0.2403,0.2294,0.3062,0.2242]';%权重4
W=[W1,W2,W3,W4];   
[~,R2] = corr(W,'type','Kendall'); 
R2(isnan(R2))=0;
R=R2(1,2:end);
W1=W';
if sum(R)<0.05*length(R)
    Wc=sum(W1)/sum(sum(W1));
    disp('一致性检验通过')
else
    disp('一致性检验不通过')
    the=std(W);  %矛盾性标准差计算
    r=corr(W);
    r(isnan(r))=0;%把NAN的元素都替换成0
    r(logical(eye(size(r))))=1;
    f=sum(1-r);%信息承载量
    c=the.*f;
    w=c/sum(c); %计算权重
    Wc1=w.*W;
    Wc=sum(Wc1');
end
%计算得分
s=data1*Wc';
Score=100*s/max(s);
for i=1:length(Score)
    fprintf('方案%d百分制评分为：%4.2f\n',i,Score(i));   
end