clc;clear;close all;
load('heart_data.mat');
data=heart_data;
train_num=round(0.8*size(data,1));%取整个数据0.8的比例训练
train_data=data(1:train_num,:);
test_data=data(train_num+1:end,:);
n=size(heart_data,2);
y=train_data(:,n);
x=train_data(:,1:n-1);
y_t=test_data(:,n);
x_t=test_data(:,1:n-1);
 GM= fitglm(x,y);
 y_p = predict(GM,x_t);
 l_data=(y_p>=0.5);
accu=1-sum(abs(l_data-y_t))./length(y_p);
