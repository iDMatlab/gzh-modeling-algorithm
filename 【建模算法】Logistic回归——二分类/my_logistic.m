clc;clear;close all;
load('heart_data.mat');
data=heart_data;
train_num=round(0.8*size(data,1));%取整个数据0.8的比例训练
train_data=data(1:train_num,:);
test_data=data(train_num+1:end,:);
n=size(heart_data,2);
y=train_data(:,n);
x=train_data(:,1:n-1);
y_t=test_data(:,n);
x_t=test_data(:,1:n-1);
x1=[ones(size(x,1),1),x];%在基础上增加常数项
x_t1=[ones(size(x_t,1),1),x_t];
w=zeros(size(x1,2),1);
lr=0.001;%学习率
batch_size=500;
%随机取数每次训练结果可能不一样
epoch=100000;  %epoch=1000000 accu=0.8195
for i=1:epoch
    idc=randperm(length(y),batch_size);
    bat_x=x1(idc,:);  %小批量的x
    bat_y=y(idc,:);  %小批量的y
    bat_yp=  bat_x*w;  %预测出的方程式取值
    y_gz=1./(1+exp(-bat_yp)); %预测出的y
    d_w=(bat_y-y_gz)'*bat_x/length(bat_y);
    w=w+lr*d_w';
end
y_gzt=x_t1*w;
p_last=(y_gzt>=0.5);
accu=1-sum(abs(p_last-y_t))./length(y_t);