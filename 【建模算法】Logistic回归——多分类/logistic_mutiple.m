%https://mp.weixin.qq.com/s/Kw4xB3OpDt8LtdKIql14gQ
clc;clear;close all;
load('iri_data.mat')
data=iri_data;
train_num=round(0.8*size(data,1));%取整个数据0.8的比例训练，其余作为测试数据
choose=randperm(size(data,1));
train_data=data(choose(1:train_num),:);
test_data=data(choose(train_num+1:end),:);
n=size(data,2);
y=train_data(:,n);
x=train_data(:,1:n-1);
y_t=test_data(:,n);
x_t=test_data(:,1:n-1);
n=3; %标签数量
index=cell(1,n);
X=cell(1,n);%将不同类别的数据找出来
for i=1:n
index{1,i}=find(y == i);
X{1,i}=x(index{1,i},1:end); %将不同标签的数据找出来
end
%构建 n*(n-1)/2 个分类器
trainData=cell(1,n*(n-1)/2);
trainLabel=cell(1,n*(n-1)/2);
logistic_nn=cell(1,n*(n-1)/2);
num=0;
for j=1:n-1
    for i=j+1:n
        num=num+1;
        trainData{1,num}=[X{1,j};X{1,i}];
        trainLabel{1,num}=[zeros(1,size(X{1,i},1)),ones(1,size(X{1,j},1))]';
        XX=trainData{1,num};
        YY=trainLabel{1,num};
        GM= fitglm( XX, YY);
        logistic_nn{1,num}=GM;
    end
end
%%
%进行预测
for j=1:n*(n-1)/2
    Y_pred=predict(logistic_nn{1,j},x_t);
    index11=find(Y_pred>=0.5);
    index12=find(Y_pred<0.5);
    Y_pred(index11)=1;
    Y_pred(index12)=0;
    result(:,j)=Y_pred;   
end
 %%
num=0;
for m=1:n
     for j=(m+1):n
         num=num+1;
         A(1,num)=m;
         A(2,num)=j;
     end
end
%%
preLabel=zeros(1,size(x_t,1));
for i=1:size(x_t,1)
    for j=1:n 
         index1=find(A(1,:)==j);
         index2=find(A(2,:)==j);
         AA=zeros(1,length(index1));
         for k=1:length(index1)
             AA(k)=result(i,index1(k));
         end
             BB=zeros(1,length(index2));
         for k=1:length(index2)
             BB(k)=result(i,index2(k));
         end
         CC=[AA,BB];
        if CC==[(repmat(0,length(index1),1))',(repmat(1,length(index2),1))']
            preLabel(i)=j;
        end
    end
end
accuracy = length(find((preLabel)==y_t'))/length(preLabel);