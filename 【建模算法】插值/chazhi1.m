%https://mp.weixin.qq.com/s/x4MuQcM2siI5Qh4lQzGP3A
clc;clear;close all;
data=xlsread('插值数据');
 index1{1,1}=[6,18];%对数据1将6,18位置当作测试位置
 index1{1,2}=[2,13];%%对数据2将2，13位置当作测试位置
for j=1:2
    figure(j)
    Y1=data(:,j);%数据1
    X1=(1:24);X2=(1:24);
    index=index1{1,j};
    x1=X1;x1(index)=[];
    y1=Y1;y1(index)=[];
    str={'linear','nearest','next','pchip','makima','spline'};
    str1={'线性插值','最临近插值','下一个邻点插值','分段三次插值','三次 Hermite 插值','三次样条插值'};
    for i=1:length(str)
        subplot(3,2,i)
        yy1 = interp1(x1,y1,X1, str{i});%插值得到数值
        plot(X1,Y1,'LineWidth',1.5)
        hold on
        plot(index,yy1(index),'o','LineWidth',3)
        wucha=Y1(index)-yy1(index)';
        rms=sqrt(sum(wucha.^2));
        rms1=ceil(rms);
        rm=num2str(rms1,'%d');
        str2=[str1{i},' RMES=',rm];
        title(str2)
        xlabel('time/h')
        ylabel('power/kwh')
    end
    legend('真实数据','插值得到数据')
end