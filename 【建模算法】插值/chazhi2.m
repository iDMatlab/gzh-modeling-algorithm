%%https://mp.weixin.qq.com/s/x4MuQcM2siI5Qh4lQzGP3A
clc;clear;close all;
str={'linear','nearest','next','pchip','makima','spline'};
str1={'线性插值','最临近插值','下一个邻点插值','分段三次插值','三次 Hermite 插值','三次样条插值'};
 for i=1:length(str)
     subplot(3,2,i)
     x = (-5:5)';
     v1 = x.^2-9*x+7;
     v2 =x.^3-6*x+8;
     v = [v1 v2 ];
     xq = -5:0.1:5;
     v11= xq.^2-9*xq+7;
     v12=xq.^3-6*xq+8;
     vT= [v11' v12'];%真实数据
     vq = interp1(x,v,xq,str{i});%插值数据
     plot(x,v,'o',xq,vq);
     hold on
     wucha=vq-vT;
     rms=sqrt(sum(wucha(:,1).^2));
     rms=ceil(rms);
     rm=num2str(rms,'%d');
     rms1=sqrt(sum(wucha(:,2).^2));
     rms1=ceil(rms1);
     rm1=num2str(rms1,'%d');
     str2=[str1{i},' RMES1=',rm,' RMES2=',rm1];
     title(str2)
 end