%https://mp.weixin.qq.com/s/JgsXRo_S3_bEVUfqDA7b6A
clc;clear;close all;
load('iri_data.mat')
 data=(iri_data);
%% 看数据分布
% data=ceil(iri_data);
train_num=round(0.8*size(data,1));%取整个数据0.8的比例训练，其余作为测试数据
choose=randperm(size(data,1));
train_data=data(choose(1:train_num),:);
test_data=data(choose(train_num+1:end),:);
n=size(data,2);
y=train_data(:,n);
x=train_data(:,1:n-1);
y_t=test_data(:,n);
x_t=test_data(:,1:n-1);
class_num=length(unique(y));   %分类标签数据
index=cell(1,class_num);
X=cell(1,class_num);%将不同类别的数据找出来
for i=1:class_num
    index{1,i}=find(y == i);
    X{1,i}=x(index{1,i},1:end); %将不同标签的数据找出来
end
%找到不同类别的概率
for i=1:class_num
    P_Y(i)=length(index{1,i})/length(y);
end
%找到不同指标有哪些
for i=1:n-1
    get_data=tabulate(train_data(:,i));
    zhibiao_cell{1,i}=get_data(:,1);
end
for N=1:n-1
    X_label=zhibiao_cell{1,N};
    for j=1:class_num
        test_data1=X{1,j};
        for i=1:length(X_label)
            lamda=1;
            k=length(X_label);
            P_X{1,N}(i,j)=(length(find(X_label(i)==test_data1(:,N)))+lamda)/(lamda*k+length(test_data1(:,N)));
        end
    end
end
%%
for i=1:size(test_data,1)
    get_data1=test_data(i,:);
    for  k=1:class_num
        pk=1;
%         PP=1;
        for j=1:n-1
            data_test1=abs(get_data1(j)- zhibiao_cell{1,j});
            [~,data_label]=min(data_test1);
            PP=P_X{1,j}(data_label,k)*pk;%对三个类别分别进行公式求解
            pk=PP;
        end
        PP1(k)=PP*P_Y(k);
    end
    [~,preLabel(i)]=max(PP1);  %概率的最大值即为类别
end
accuracy = length(find((preLabel)==y_t'))/length(preLabel);
%% MATLAB 自带的贝叶斯分类函数
Nb=fitcnb(x,y);
y_nb=Nb.predict(x_t);
accuracy1 = length(find((y_nb)==y_t))/length(y_t);
% C_nb=confusionmat(y_t,y_nb);
%% 以下是优化贝叶斯网络
% Mdl = fitcnb(x,y,'OptimizeHyperparameters','auto',...
%     'HyperparameterOptimizationOptions',struct('AcquisitionFunctionName',...
%     'expected-improvement-plus'));