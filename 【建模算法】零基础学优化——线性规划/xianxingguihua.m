%https://mp.weixin.qq.com/s/8_dKC_hAG5Uw9tbKB9CgMg
clc;clear;close all;
%目标函数
f=[-1;-2;3];

%不等约束
A=[-1,-1,0;0,-1,-1];%左边特征矩阵
b=[-3;-3]; %右边

%等式约束
Aeq=[1,0,1];
Beq=[4];


%变量约束,上限，下限
LB=zeros(3,1);
UB=2*ones(3,1);

%优化
[x,fval]=linprog(f,A,b,Aeq,Beq,LB,UB);

objstr=['目标函数最优值：',num2str(fval)];
disp(objstr)
for i=1:length(x)
    xstr=['x',num2str(i),'的值为：',num2str(x(i))];
    disp(xstr)
end

%%
clc;clear;close all;

%初始解
x0=zeros(3,1);

%不等约束
A=[-1,-1,0;0,-1,-1];%左边特征矩阵
b=[-3;-3]; %右边

%等式约束
Aeq=[1,0,1];
Beq=[4];

%变量约束,上限，下限
LB=zeros(3,1);
UB=2*ones(3,1);
%优化求解
fun = @(x)-x(1)-2*x(2)+3*x(3);
[x,fval]=fmincon(fun,x0,A,b,Aeq,Beq,LB,UB);

objstr=['目标函数最优值：',num2str(fval)];
disp(objstr)
for i=1:length(x)
    xstr=['x',num2str(i),'的值为：',num2str(x(i))];
    disp(xstr)
end
% function f=fun(x)
% %目标函数
% f=-x(1)-2*x(2)+3*x(3);
% end

%% yalmip 求解
clc;clear;close all;
%实型变量   intvar 整型变量   binvar 0-1型变量
p=sdpvar(3,1); 

%目标函数
Objective=-p(1)-2*p(2)+3*p(3);

%约束条件
Constraints=[(0<=p<=2),((p(1)+p(2))>=3),((p(2)+p(3))>=2),((p(1)+p(3))==4)];
%优化求解
optimize(Constraints,Objective)

P=double(p);
Obj=double(Objective);
objstr=['目标函数最优值：',num2str(Obj)];
disp(objstr)
for i=1:length(P)
    xstr=['x',num2str(i),'的值为：',num2str(P(i))];
    disp(xstr)
end