%% yalmip 求解
clc;clear;close all;
%sdpvar实型变量   intvar 整形变量   binvar 0-1型变量
p=binvar(3,1); 

%目标函数
Objective=-p(1)^2-p(2)*p(3);

%约束条件
Constraints=[(p(1)^2+p(1)*p(2)+p(2)*p(3)<=p(3)+5),((3*p(1)+2*p(3))<=4)];
%优化求解
optimize(Constraints,Objective)

P=double(p);
Obj=double(-Objective);
objstr=['目标函数最优值：',num2str(Obj)];
disp(objstr)
for i=1:length(P)
    xstr=['x',num2str(i),'的值为：',num2str(P(i))];
    disp(xstr)
end

%% yalmip 求解
clc;clear;close all;
%sdpvar实型变量   intvar 整形变量   binvar 0-1型变量
p=sdpvar(3,1); 

%目标函数
Objective=-p(1)^2+p(2)^2-p(2)*p(3);

%约束条件
Constraints=[0<=p<=1,(p(1)^2+p(1)*p(2)+p(2)*p(3)<=p(2)+6),((2*p(1)+p(2)+3*p(3))<=6)];
%优化求解
optimize(Constraints,Objective)

P=double(p);
Obj=double(-Objective);
objstr=['目标函数最优值：',num2str(Obj)];
disp(objstr)
for i=1:length(P)
    xstr=['x',num2str(i),'的值为：',num2str(P(i))];
    disp(xstr)
end
%%
%%
clc;clear;close all;
%初始解
x0=zeros(3,1);

%不等约束
A=[2,1,3];%左边特征矩阵
b=[6]; %右边

%等式约束
Aeq=[];
Beq=[];

%变量约束,上限，下限
LB=zeros(3,1);
UB=1*ones(3,1);
%优化求解
fun = @(x)-x(1)^2+x(2)^2-x(2)*x(3);
nonlcon = @unitdisk;
[x,fval]=fmincon(fun,x0,A,b,Aeq,Beq,LB,UB,nonlcon);

objstr=['目标函数最优值：',num2str(-fval)];
disp(objstr)
for i=1:length(x)
    xstr=['x',num2str(i),'的值为：',num2str(x(i))];
    disp(xstr)
end

function [c,ceq] = unitdisk(x)
%c为不等式非线性约束
%ceq为等式非线性约束
c=x(1)^2+x(1)*x(2)+x(2)*x(3)-x(2)-6;
ceq = [];
end