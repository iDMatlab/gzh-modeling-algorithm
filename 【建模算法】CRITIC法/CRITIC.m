%https://mp.weixin.qq.com/s/PZnwNX8IiyvCMs7ec9MUSw
clc;clear;
data=xlsread('银行数据.xlsx');
%指标正向化和标准化处理后数据为data1
data1=data;
%%负向指标准化处理,
index=[3];    %第三个指标为负向指标
for i=1:length(index)
  data1(:,index(i))=(max(data(:,index(i)))-data(:,index(i)))/(max(data(:,index(i)))-min(data(:,index(i))));
end
%%正向指标准化处理
index_all=1:size(data1,2); 
index_all(index)=[];    % 除负向指标外其余所有指标
index=index_all;
for i=1:length(index)
  data1(:,index(i))=(data(:,index(i))-min(data(:,index(i))))/(max(data(:,index(i)))-min(data(:,index(i))));
end
%%对比性
the=std(data1);
%%矛盾性
r=corr(data1);%计算指标间的相关系数
f=sum(1-r);
%%信息承载量
c=the.*f;
%计算权重
w=c/sum(c);
%计算得分
s=data1*w';
Score=100*s/max(s);
yin={'中信','光大','浦发','招商'};
for i=1:length(Score)
    fprintf('%s银行百分制评分为：%4.2f\n',yin{1,i},Score(i));   
end