function sfcrime_draw_locs(tbl,latlim,lonlim,nbins,topN,color)
xbinedges = linspace(lonlim(1),lonlim(2),nbins);
ybinedges = linspace(latlim(1),latlim(2),nbins);
[N,~,~] = histcounts2(tbl.X, tbl.Y, xbinedges, ybinedges);
sortedN = sort(N(:), 'descend');
for ii = 1:topN
    [i,j] = find(N == sortedN(ii));
    if length(i) == 1
        loc.lon = [xbinedges(i), xbinedges(i), ...
            xbinedges(i+1), xbinedges(i+1), xbinedges(i)];
        loc.lat = [ybinedges(j), ybinedges(j+1), ...
            ybinedges(j+1), ybinedges(j), ybinedges(j)];
        linem(loc.lat, loc.lon, 'LineStyle', '-', 'Color', color)
        if ii == 1
            textm(loc.lat(1), loc.lon(1), 'p', 'Color', color)
        end
    else
        for k = 1:length(i)
            loc.lon = [xbinedges(i(k)), xbinedges(i(k)),xbinedges(i(k)+1), xbinedges(i(k)+1), xbinedges(i(k))];
            loc.lat = [ybinedges(j(k)), ybinedges(j(k)+1),ybinedges(j(k)+1), ybinedges(j(k)), ybinedges(j(k))];
            linem(loc.lat, loc.lon, 'LineStyle', '-', 'Color', color)
        end
    end
end
end

