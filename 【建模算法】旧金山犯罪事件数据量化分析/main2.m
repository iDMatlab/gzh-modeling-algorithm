%https://mp.weixin.qq.com/s/_mgwRHE-jhLcCBkXAJDuJw
%修图后
clc;clear;close all
T = readtable('train.csv','Format','%D%C%q%C%C%q%q%f%f');   % 加载train.csv 
week = {'Sunday','Monday','Tuesday','Wednesday', 'Thursday','Friday','Saturday'}; % 时间
T.(4) = reordercats(T.(4), week);                           % 重新排序类别
T.(6) = categorical(T.(6));                                 % 分类
T.Properties.VariableNames                                  % 显示变量名


%以每周为间隔进行时间序列分析。
t = datetime('2003-1-5') + days(0:7:4515);                  % 每周日期间隔
weeks = NaT(size(T.Dates));                                 % 空日期时间数组
for i = 1:length(t) - 1                                     % 每周间隔循环
    weeks(T.Dates >= t(i) & T.Dates < t(i+1)) = t(i);      
end

% “类别”中包含：犯罪和非犯罪事件有 38 类，例如纵火、袭击和空头支票 ,对犯罪数据进行排序整理
T.Week = weeks;      
T.Category = mergecats(T.Category,{'TRESPASS','TREA'});     % 合并标签错误的类别
tab = tabulate(T.Category);                                 % 列表类别
[Count, idx] = sort(cell2mat(tab(:,2)),'descend');          % 按类别总计排序
figure                                                      
bar(Count)                                                  % 画柱状图
ax = gca;                                                   % 获取当前句柄  get current axes handle
ax.XTick = 1:size(tab, 1);                                  % 使用类别作为刻度 
ax.XTickLabel = tab(idx,1);                                 % 重命名刻度标签
ax.XTickLabelRotation = -90;  
ax.FontName='times new roman';                              %时代新罗马字体
set(gca, 'Box','off' , ...
  'TickDir','out', ...
  'TickLength',[.01 .01] , ...
  'XMinorTick','on', ...
  'YMinorTick','on', ...
  'YGrid','on',...
  'XColor', [0.3 0.3 0.3], ...
  'YColor', [0.3 0.3 0.3], ...
  'LineWidth', 1);



% sfcrime_load_map                                            % load map from WMS

load data.mat %导入数据地图数据
vandalism = T(T.Category == 'VANDALISM', [1,3:5,8:10]);     % 按类别分类的子集 T
nbins = 100;                                                % 
xbinedges = linspace(lim.lon(1),lim.lon(2),nbins);          % 经度
ybinedges = linspace(lim.lat(1),lim.lat(2),nbins);          % 经度
map = flipud(A);                                            % 翻转图像
figure                                                      
colormap jet                                              % 设置颜色图
histogram2(vandalism.X, vandalism.Y, ...                    % 绘制 3D 直方图
    xbinedges, ybinedges, ...                             
    'FaceColor', 'flat', ...
    'FaceAlpha', 0.7, 'EdgeAlpha', 0.5)
hold on                                                    
image(lim.lon,lim.lat,map)                         % 添加地图
hold off                                                 
ax = gca;                                          % 获取当前句柄
ax.CLim = [0 20];                                  % 色轴缩放
title({'旧金山犯罪地图'; ...                       % 添加标题
    '时间: Jan 2003 - May 2015'})
zlabel('警方报告的计数')                           % 添加标签
text(-122.52,37.82, 300, '金门大桥','FontName','宋体')     % 标注地标
text(-122.4,37.82, 200, '海湾大桥','FontName','宋体')      % 标注地标
ax.FontName='宋体'; 




figure                                                      
usamap(lim.lat, lim.lon);                                   % 设置地图坐标
geoshow(A, R)                                               % 显示地图
sfcrime_draw_locs(vandalism,lim.lat,lim.lon,nbins,50,'r')   % 绘制前50 犯罪高发地点
title({'时间: Jan 2003 - May 2015';'前50 犯罪高发位置点'})% 添加标题

% 犯罪热力图
cats = categories(T.Category);                              % 提取类别
rawCounts = zeros((nbins-1)^2,length(cats));                % 设置一个累加器
for i = 1:length(cats)                                      % 循环分类
    data = T(T.Category == cats{i}, 8:9);                   % 分类
    [N,~,~] = histcounts2(data.X, data.Y, ...               % 直方图
        xbinedges, ybinedges);                              % 计数
    rawCounts(:,i) = N(:);                                  % 循环添加
end                                                       
figure                                                      
imagesc(rawCounts)                                          % 矩阵热力图
ax = gca;                                                   % 获得当前句柄
ax.CLim = [0 200];                                          % 颜色设定范围
ax.XTick = 1:length(cats);                                  % 使用类别作为刻度
ax.XTickLabel = cats;                                       % 重命名刻度标签
ax.XTickLabelRotation = -90;                                % 垂直旋转文本
ylabel('地点')                                         % 添加轴标签
title('犯罪热力图')                      % 添加标题
colormap jet
colorbar                                 

% pca 
%===================================
w = 1 ./ var(rawCounts);                                    
[wcoeff, score, latent, tsquared, explained] = pca(rawCounts, 'VariableWeights', w);        % weighted PCA with w
coefforth = diag(sqrt(w)) * wcoeff;                         % 将 wcoeff 转换为正交
labels = cats;                                              % 类别作为标签
labels([4,9,10,12,13,15,18,20,21,23,25,27,28,31,32]) = {''};% 删除一些标签以避免混乱
figure                                                      
biplot(coefforth(:,1:2), 'Scores', score(:,1:2), ...        % 合成两个双图
    'VarLabels', labels)
xlabel(sprintf('Component 1 (%.2f%%)', explained(1)))       % 添加x 轴标签
ylabel(sprintf('Component 2 (%.2f%%)', explained(2)))       % 添加y 轴标签
axis([-0.1 0.6 -0.3 0.4]);                                  % 定义轴限制
title({'Principal Components Analysis'; ...                 % 添加标题
    sprintf('Variance Explained %.2f%%',sum(explained(1:2)))})
htext = findobj(gca,'String','VEHICLE THEFT');              % 查找text 对象
htext.HorizontalAlignment = 'right';                        % 更改文本对齐方式
htext = findobj(gca,'String','ASSAULT');                    % 查找text 对象
htext.Position = [0.2235 0.0909 0];                         % 移动标签位置
htext = findobj(gca,'String','ROBBERY');                    % 查找text 对象
htext.Position = [0.2148 0.1268 0];                         % 移动标签位置
htext = findobj(gca,'String','ARSON');                      % 查找text 对象
htext.HorizontalAlignment = 'right';                        % 更改文本对齐方式
htext = findobj(gca,'String','EXTORTION');                  % 查找text 对象
htext.HorizontalAlignment = 'right';       


%=====================================
assault = T(T.Category == 'ASSAULT', [1,3:5,8:10]);         % 按子集T  类别分类的
vehicle = T(T.Category == 'VEHICLE THEFT', [1,3:5,8:10]);   % 按子集T  类别分类的
robbery = T(T.Category == 'ROBBERY', [1,3:5,8:10]);         % 按子集T  类别分类的

figure                                                  
usamap(lim.lat, lim.lon);                                   % 设置地图坐标
geoshow(A, R)                                               % 显示地图
topN = 50;                                                  % 设置前50名
hold on                                                     
sfcrime_draw_locs(assault,lim.lat,lim.lon,nbins,topN,'r')   % 用红色绘制位置
sfcrime_draw_locs(vehicle,lim.lat,lim.lon,nbins,topN,'g')   % 用绿色绘制位置
sfcrime_draw_locs(robbery,lim.lat,lim.lon,nbins,topN,'b')   % 用蓝色绘制位置
hold off                                                    % 恢复默认
title({'Assault, Robbery, and Vehicle Theft'; ...           % 添加标题
    sprintf('Top %d Locations',topN)})

%=========================================
figure                                                      
[G, t] = findgroups(assault.Week);                          % 按每周间隔分组
weekly = splitapply(@numel, assault.Week, G);               % 每周数目统计
plot(t, weekly)                                             % 每周计数绘图
hold on                                                   
[G, t] = findgroups(vehicle.Week);                          % 按每周间隔分组
weekly = splitapply(@numel, vehicle.Week, G);               % 每周数目统计
plot(t, weekly)                                             % 每周计数绘图
[G, t] = findgroups(robbery.Week);                          % 按每周间隔分组
weekly = splitapply(@numel,robbery.Week, G);                % 获取每周数目
plot(t, weekly)                                             % 每周计数绘图
hold off                                                    % 恢复默认
title('ASSAULT, ROBBERY, VEHICLE THEFT - Weekly')           % 添加标题
ylabel('Count of Incidence Reports')                        % 添加标签
legend('ASSAULT','VEHICLE THEFT', 'ROBBERY')                % 添加图例


%=============================================
figure
isRecovered = strfind(vehicle.Descript,'RECOVERED');        %  在描述中找到“RECOVERED”
isRecovered = ~cellfun(@(x) isempty(x), isRecovered);       % 如果不为空则恢复
[G, t] = findgroups(vehicle.Week(~isRecovered));            % 按每周间隔分组
weekly = splitapply(@numel, vehicle.Week(~isRecovered), G); % 获取每周数目
plot(t, weekly)                                             % 每周计数绘图
hold on
[G, t] = findgroups(vehicle.Week(isRecovered));             % 按每周间隔分组
weekly = splitapply(@numel, vehicle.Week(isRecovered), G);  % 获取每周数目
plot(t, weekly)                                             % 每周计数绘图
hold off                                                    % 恢复默认
title('VEHICLE THEFT - Weekly')                             % 添加标题添加坐标轴标签
legend('UNRECOVRED', 'RECOVERED')                           % 添加图例


% 另一种破窗理论
%==================================
larceny = T(T.Category == 'LARCENY/THEFT', [1,3:5,8:10]);   
isAuto = strfind(larceny.Descript,'LOCKED');                
isAuto = ~cellfun(@(x) isempty(x), isAuto);                 
figure                                                     
subplot(2,1,1)                                             
[G, t] = findgroups(larceny.Week(isAuto));                
weekly = splitapply(@numel, larceny.Week(isAuto), G);       
plot(t, weekly)                                             
title('LARCENY/THEFT, Auto')                                
subplot(2,1,2)                                             
[G, t] = findgroups(larceny.Week(~isAuto));                 
weekly = splitapply(@numel, larceny.Week(~isAuto), G);    
plot(t, weekly)                                             
title('LARCENY/THEFT, Non-Auto')                           
ylim([0 500])                                               
hold off    

%===========================================
figure                                                     
usamap(lim.lat, lim.lon);                                   % 设置地图坐标
geoshow(A, R)                                               % 显示地图
topN = 100;                                                 % 获取前100
hold on                                                    
sfcrime_draw_locs(larceny(isAuto,:),lim.lat,lim.lon, ...    % 用蓝色绘制红色
    nbins,topN,'r')
sfcrime_draw_locs(vehicle,lim.lat,lim.lon,nbins,topN,'b')   % 用蓝色绘制位置
plotm(37.802139, -122.41874, '+g')                          % 添加地标
plotm(37.808119, -122.41790, '+g')                         
plotm(37.7808297, -122.4301075, '+g')                      
plotm(37.7842048, -122.3969652, '+g')                       
plotm(37.7786559, -122.5106296, '+g')                      
plotm(37.8038433, -122.4418518, '+g')                      
hold off                                                  
title({'LARCENY/THEFT - Auto vs. VEHICLE THEFT'; ...        
    sprintf('Top %d Locations',topN)})
textm(37.802139, -122.41574, 'Lombard Street', ...          % 标注地标
    'Color', 'g')
textm(37.8141187, -122.43450, 'Fisherman''s Wharf', ...     
    'Color', 'g')
textm(37.7808297, -122.4651075, 'Japan Town', ...           
    'Color', 'g')
textm(37.7842048, -122.3949652, 'SoMa', ...                
    'Color', 'g')
textm(37.7786559, -122.5086296, 'Sutro Baths', ...          
    'Color', 'g')
textm(37.8088629, -122.4628518, 'Marina District', ...     
    'Color', 'g')
textm(37.715564, -122.475662, ...                           
    {'Red:  LARCENY/THEFT - Auto','Blue: VEHICLE THEFT'}, 'BackgroundColor','w')