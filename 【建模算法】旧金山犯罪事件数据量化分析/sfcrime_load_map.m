% server :https://apps.nationalmap.gov/services/
% https://coastwatch.pfeg.noaa.gov/erddap/wms/documentation.html

% 三种 地图风格
%serverURL='http://svs.gsfc.nasa.gov/cgi-bin/wms?';
% serverURL ='https://basemap.nationalmap.gov/arcgis/services/USGSHydroCached/MapServer/WMSServer?';
% serverURL ='https://basemap.nationalmap.gov/arcgis/services/USGSTNMBlank/MapServer/WMSServer?';
%  serverURL ='https://basemap.nationalmap.gov/arcgis/services/USGSImageryOnly/MapServer/WMSServer?';
% serverURL='https://basemap.nationalmap.gov/arcgis/services/USGSShadedReliefOnly/MapServer/WMSServer?';
% serverURL='https://coastwatch.pfeg.noaa.gov/erddap/wms/jplG1SST/request?';
% serverURL='https://basemap.nationalmap.gov/arcgis/services/USGSImageryTopo/MapServer/WMSServer?';% 可以用
% serverURL='https://basemap.nationalmap.gov/arcgis/services/USGSHydroCached/MapServer/WMSServer?';% 可以用
serverURL='https://basemap.nationalmap.gov/arcgis/services/USGSImageryOnly/MapServer/WMSServer?';% 可以用
% serverURL='https://coastwatch.pfeg.noaa.gov/erddap/wms/jplMURSST41/request?';% 可以用
% serverURL='http://earth.dot.ca.gov/geoserver/wms?';
% layers = wmsfind('jplg1sst','SearchField','serverurl');
% layers = wmsfind('BlueMarbleNG','SearchFields','layername', 'MatchType','exact');
% layers = wmsfind('*.gov*','SearchFields','serverurl');
% urls = servers(layers);
% serverURL = layers(1).ServerURL;
lim.lat = [37.7055362 37.8274702];           
lim.lon = [-122.5193077 -122.3576067];
img.h = 420 * 2;                                      
img.w = 560 * 2;    
sf_lat = 37.7577627;
sf_lon = -122.4397891;                                
wms = wmsinfo(serverURL);                                                                                       
layer = wms.Layer;                                      
[A,R] = wmsread(layer, 'ImageFormat', 'image/png', ...  
'Lonlim', lim.lon, 'Latlim', lim.lat, ...
    'ImageHeight', img.h, 'ImageWidth', img.w);
save data