%https://mp.weixin.qq.com/s/rUo_GY2LHgsUNNoXFpNiFg
clc;clear;close all;
load('abalone_data.mat')
n=size(data,2);
x=data(:,1:n-1); %自变量 
y=data(:,n);%因变量
x1=x;%扩展变量
%% 看一眼自变量和因变量的关系
%定义一下颜色，来个花里胡哨的散点图
figure(1)
color=[207,155,255;128,199,252;255,193,151;...
    254	,168,217;112,138,248;184,84,246;234,168,135;111,168,86]/255;
for i=1:n-1
    subplot(4,2,i)
    plot(data(:,i),y,'*','Color',color(i,:))
    str=['自变量变量',num2str(i)];
    title(str)
end
for i=1:4
    if (i==1)
        x1=x;%原变量
    elseif (i==2)
        %修正变量
        x1(:,2)=x(:,2).^2;
        x1(:,3)=x(:,3).^2;
        x1(:,4)=x(:,4).^3;
        x1(:,5)=x(:,5).^(1/2);
        x1(:,6)=x(:,6).^(1/2);
        x1(:,7)=x(:,7).^(1/2);
        x1(:,8)=x(:,8).^(1/2);
      elseif (i==3)
          %原变量+修正变量
        x1=[x1,x(:,2:end)];
       elseif (i==4)
     % 拓展变量x1x2,x1x3,x2x3....,x14x15%原变量+修正变量+交叉变量
        for m=2:size(x,2)
            for n=m+1:size(x,2)
                xx=x(:,m).*x(:,n);
                x1=[x1,xx];
            end
        end 
    end
    %% 画图
    figure(2)
    titlestr={'原变量  ','修正变量  ','原变量+修正变量  ','原变量+修正变量+交叉变量  '};
    subplot(2,2,i)
    x2=ones(size(x1,1),1);%构建一个全为1的变量
    x2=[x2,x1];
    xishu2=inv(x2'*x2)*x2'*y;%原因变量系数
    y_n=xishu2(1)+x2(:,2:end)*xishu2(2:end);
    wucha=sum(abs(y_n-y)./y)/length(y);
    titlestr1=[titlestr{1,i},'相对误差：',num2str(wucha)];
    plot(y(3000:length(y)),'Color',color(2*i,:),'LineWidth',1,'DisplayName','真实数据')
    hold on
    plot(y_n(3000:length(y)),'*','Color',color(i,:),'DisplayName','拟合数据')
     title(titlestr1)
    legend('show','Location','Best');
end