%https://mp.weixin.qq.com/s/_6SiTL3QNyFykJNfzqKOZw
clc;clear;close all;
load('abalone_data.mat')
n=size(data,2);
x=data(:,1:n-1);y=data(:,n);
x1=[ones(size(x,1),1),x];
beta=ones(size(x1,2),1);%回归系数
%% 坐标轴下降法求解
epochs=1000;%迭代次数
learnrate=0.001;%学习率
lamda=0.003;
for j=1:epochs
    beta1=beta;%记录上一轮的bata
    for i=1:length(beta)
        for n=1:epochs
    %找到让损失函数收敛的点    
             yn=x1*beta;
             J=x1(:,i)'*(yn-y)/length(y)+lamda*sign(beta(i));  
             beta(i)=beta(i)-J*learnrate;
             if (abs(J)<1e-3)
                 break
             end
        end
    end
     if(sum(abs(beta1-beta)<1e-3)==sum(length(beta)))
%      if((abs(beta1-beta)<1e-3))
         break
     end
end
figure(1)
y_p=x1*beta;
color1=[184	184	246]/255;
color2=[255	193	151]/255;
wucha=sum(abs(y_p-y)./y)/length(y);
titlestr=['坐标轴下降法求解  lamda系数为',num2str(min(lamda)),'误差为：',num2str(wucha)];
titlestr1=['迭代轮数为',num2str(epochs),'   学习率为',num2str(learnrate)];
plot(y(3000:end),'Color',color1,'LineWidth',1)
hold on
plot(y_p(3000:end),'*','Color',color2)
hold on
legend('真实数据','回归拟合数据')
title({titlestr;titlestr1})
%% 近似梯度下降法求解
epoch1=1000;
lamda=0.003;
rate1=0.001;
beta1=ones(size(x1,2),1);%回归系数
for i=1:epoch1
    yn1=x1*beta1;
    grad=x1'*(yn1-y)/length(y)+lamda*sign(beta1);
    beta1=beta1-rate1*grad;
end
y_p2=x1*beta1;
wucha2=sum(abs(y_p2-y)./y)/length(y);
figure(2)
color1=[111	168	86]/255;
color2=[234	168	135]/255;
titlestr=['近似梯度下降法求解  lamda系数为',num2str(min(lamda)),'  误差为：',num2str(wucha2)];
titlestr1=['迭代轮数为',num2str(epoch1),'   学习率为',num2str(rate1)];
plot(y(3000:length(y)),'Color',color1,'LineWidth',1)
hold on
plot(y_p2(3000:length(y)),'*','Color',color2)
hold on
legend('真实数据','回归拟合数据')
title({titlestr;titlestr1})
%% 与matlab封装的lasso函数对比效果
[B,FitInfo] = lasso(x,y, 'Lambda',lamda);
y_p1=x*B+FitInfo.Intercept;
wucha1=sum(abs(y_p1-y)./y)/length(y);
figure(3)
color1=[112	138	248]/255;
color2=[254	168	217]/255;
titlestr=['MATLAB封装函数lasso求解  lamda系数为',num2str(min(lamda)),'  误差为：',num2str(wucha1)];
plot(y(3000:length(y)),'Color',color1,'LineWidth',1)
hold on
plot(y_p1(3000:length(y)),'*','Color',color2)
hold on
legend('真实数据','回归拟合数据')
title(titlestr)
