%https://mp.weixin.qq.com/s/_6SiTL3QNyFykJNfzqKOZw
clc;clear;close all;
load('abalone_data.mat')
n=size(data,2);
x=data(:,1:n-1);y=data(:,n);
Alpha=0.75;
[B,FitInfo] = lasso(x,y, 'CV' ,10,'Alpha',Alpha);
%'CV' ,10 交叉验证,
%'Alpha',0.75 设置弹性网系数，'Alpha',0 为纯岭回归 ，'Alpha',1为纯lasso回归
lassoPlot(B,FitInfo, 'PlotType' , 'CV' );
legend( 'show' ) % 显示图例
figure(1)
idxLambda1SE = FitInfo.Index1SE;
coef = B(:,idxLambda1SE);%回归系数
coef0 = FitInfo.Intercept(idxLambda1SE);%常系数
yn= coef0 +x*coef;
color1=[234	168	135]/255;
color2=[160	207	220]/255;
wucha=sum(abs(y-yn)./y)/length(y);
figure(2)
lanmda=FitInfo.LambdaMinMSE;
titlestr=['Alpha取值为',num2str(Alpha),'  交叉验证Lamda取值为：',num2str(lanmda),'   误差为：',num2str(min(wucha))];
plot(y(3000:end),'Color',color1,'LineWidth',1.5)
hold on
plot(yn(3000:end),'*','Color',color2)
hold on
legend('真实数据','回归拟合数据')
title(titlestr)