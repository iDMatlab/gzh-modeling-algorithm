%%https://mp.weixin.qq.com/s/N-wRm4Dg4_Msjd0KPCJ8TA
clc;clear;close all;
load('iri_data.mat')   
data=iri_data;
num_train = ceil(0.8*size(iri_data,1));%整个数据的0.8作为训练集
choose = randperm(length(data));
train_data = data(choose(1:num_train),1:end); %训练集
label_train = train_data(:,end);   %训练标签
test_data = data(choose(num_train+1:end),1:end); %测试集
label_test = test_data(:,end);   %测试标签
SVMModel = fitcecoc(train_data(:,1:end-1),label_train);
[~,score] = predict(SVMModel,test_data(:,1:end-1));
[~,label_pre] = max(score');
accuracy = length(find(label_pre==label_test'))/length(label_test);
disp('SVM多分类正确率为')
disp(accuracy)

%%  交叉验证和混淆矩阵
CVMdl = crossval(SVMModel);
oofLabel = kfoldPredict(CVMdl);
ConfMat = confusionchart(label_train,oofLabel,'RowSummary','total-normalized');
ConfMat.InnerPosition = [0.10 0.12 0.85 0.85];

%% 优化SVM代码
clc;clear;close all;
load('iri_data.mat')   
data=iri_data;
num_train = ceil(0.8*size(iri_data,1));%整个数据的0.8作为训练集
choose = randperm(length(data));
train_data = data(choose(1:num_train),1:end); %训练集
label_train = train_data(:,end);   %训练标签
test_data = data(choose(num_train+1:end),1:end); %测试集
label_test = test_data(:,end);   %测试标签
SVMModel = fitcecoc(train_data(:,1:end-1),label_train,'OptimizeHyperparameters','auto',...
    'HyperparameterOptimizationOptions',struct('AcquisitionFunctionName',...
    'expected-improvement-plus'));
[~,score] = predict(SVMModel,test_data(:,1:end-1));
[~,label_pre] = max(score');
accuracy = length(find(label_pre==label_test'))/length(label_test);
disp('SVM多分类正确率为')
disp(accuracy)