%%https://mp.weixin.qq.com/s/N-wRm4Dg4_Msjd0KPCJ8TA
clc;clear;close all;
load('iri_data.mat')   
data=iri_data;
num_train = ceil(0.8*size(iri_data,1));%整个数据的0.8作为训练集
choose = randperm(length(data));
train_data = data(choose(1:num_train),1:end); %训练集
label_train = train_data(:,end);   %训练标签
test_data = data(choose(num_train+1:end),1:end); %测试集
n1=size(data,2);           %指标数量
y=train_data(:,n1);
x=train_data(:,1:n1-1);
y_t=test_data(:,n1);
x_t=test_data(:,1:n1-1);
n=length(unique(y)); %标签数量，即类别数
index=cell(1,n);
X=cell(1,n);%将不同类别的数据找出来
for i=1:n
index{1,i}=find(y == i);
X{1,i}=x(index{1,i},1:end); %将不同标签的数据找出来
end
%构建 n*(n-1)/2 个分类器
trainData=cell(1,n*(n-1)/2);
trainLabel=cell(1,n*(n-1)/2);
SVM_Model=cell(1,n*(n-1)/2);
num=0;
for j=1:n-1
    for i=j+1:n
        num=num+1;
        trainData{1,num}=[X{1,j};X{1,i}];
        trainLabel{1,num}=[zeros(1,size(X{1,i},1)),ones(1,size(X{1,j},1))]';
        XX=trainData{1,num};
        YY=trainLabel{1,num};
        SVMModel = fitcsvm(XX,YY,'KernelScale','auto','Standardize',true,...
        'OutlierFraction',0.05);
        SVM_Model{1,num}=SVMModel;
    end
end
%对测试集进行预测
for j=1:n*(n-1)/2
    Y_pred=predict(SVM_Model{1,j},x_t);
    index11=find(Y_pred>=0.5);
    index12=find(Y_pred<0.5);
    Y_pred(index11)=1;
    Y_pred(index12)=0;
    result(:,j)=Y_pred;   
end
 %构造分类器矩阵
num=0;
for m=1:n
     for j=(m+1):n
         num=num+1;
         A(1,num)=m;
         A(2,num)=j;
     end
end
label_last_result=zeros(1,size(x_t,1));  
%类别判断
for i=1:n
if (i==1) %类别1的情况
    index1=find(A(1,:)==i);
    result_label=(result(:,index1)==zeros(1,length(index1)));
    label_last=find(sum(result_label')==n-1);
    label_last_result(label_last)=i;
elseif (i==n) %类别n的情况
    index2=find(A(2,:)==i);
    result_label=(result(:,index2)==ones(1,length(index2)));
    label_last=find(sum(result_label')==n-1);
    label_last_result(label_last)=i;
else
    index1=find(A(1,:)==i);
    index2=find(A(2,:)==i); 
    index_com=[zeros(1,length(index1)),ones(1,length(index2))];
    result_label=(result(:,[index1,index2])==index_com);
    label_last=find(sum(result_label')==n-1);
    label_last_result(label_last)=i;
end
end
accuracy = length(find((label_last_result)==y_t'))/length(label_last_result);
disp('测试集准确率为：')
disp(accuracy)
