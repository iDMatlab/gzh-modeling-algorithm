%https://mp.weixin.qq.com/s/N-wRm4Dg4_Msjd0KPCJ8TA
clc;clear;close all;
load('heart_data')    %数据 和logistic二分类那篇推文
data=heart_data;
data(:,end)=data(:,end)+1;  %注意 0-1标签数据要做成1-2标签的
num_train = ceil(0.8*size(heart_data,1));%整个数据的0.8作为训练集
choose = randperm(length(data));
train_data = data(choose(1:num_train),1:end); %训练集
label_train = train_data(:,end);   %训练标签
test_data = data(choose(num_train+1:end),1:end); %测试集
label_test = test_data(:,end);   %测试标签
%% 通过使用自动超参数优化，找到最小化五折交叉验证损失的超参数
SVMModel =fitcsvm(train_data(:,1:end-1),label_train,'OptimizeHyperparameters','auto', ...
    'HyperparameterOptimizationOptions',struct('AcquisitionFunctionName', ...
    'expected-improvement-plus'));
[~,score] = predict(SVMModel,test_data(:,1:end-1));
[~,label_pre] = max(score');
accuracy = length(find(label_pre==label_test'))/length(label_test);
disp('SVM二分类正确率为')
disp(accuracy)
%%
save SVMModel