clc;clear; close all;
%%
load('abalone_data.mat')%鲍鱼数据
global input_num hidden_num output_num input_data output_data
% 导入数据
%设置训练数据和测试数据
[m,n]=size(data);
train_num=round(0.8*m); %自变量 
x_train_data=data(1:train_num,1:n-1);
y_train_data=data(1:train_num,n);
%测试数据
x_test_data=data(train_num+1:end,1:n-1);
y_test_data=data(train_num+1:end,n);
x_train_data=x_train_data';
y_train_data=y_train_data';
x_test_data=x_test_data';
% 标准化
[x_train_regular,x_train_maxmin] = mapminmax(x_train_data);
[y_train_regular,y_train_maxmin] = mapminmax(y_train_data);
input_data=x_train_regular;
output_data=y_train_regular;

input_num=size(x_train_data,1); %输入特征个数
hidden_num=6;   %隐藏层神经元个数
output_num=size(y_train_data,1); %输出特征个数
num_all=input_num*hidden_num+hidden_num+hidden_num*output_num+output_num;%网络总参数，只含一层隐藏层;
%自变量的个数即为网络连接权重以及偏置
popmax=3;   %自变量即网络权重和偏置的上限
popmin=-3;  %自变量即网络权重和偏置的下限
SearchAgents_no=50; % Number of search agents  搜索麻雀数量
Max_iteration=100; % Maximum numbef of iterations 最大迭代次数
bestX=[0.267215859919525	-0.501774496177196	2.65401202107707	-0.646338504598494	-1.92997082644040	2.90211811871954	-0.300139827813571	-2.53308867577880	-2.39177630663167	2.26710766929880	0.236484301684038	2.89219740091749	-2.53306982818767	2.80521169034645	0.493446957001528	-2.41036826593340	0.706836514882977	-2.24919324650154	0.0858351195197096	2.32581652201184	0.0913957614946499	-2.33673367042421	-1.96059483190087	-0.850819551445352	-0.302911292277069	2.36128512198285	-2.89583914243150	-0.596192967035660	1.08338885727805	0.0225189750594272	-2.23599277919844	1.51725798069496	-1.16948783586583	-1.67117942224155	-1.69349681832665	0.611657095855844	0.0934483844135459	-2.14426038739748	-1.72326017259123	-1.58618355438005	-2.47254420405327	1.06819875748127	-0.508972247808367	-1.30801447738035	2.21696472264025	0.334402799138466	0.299769231897001	-2.82909351223011	2.19974006207349	0.398048871058118	0.571155159731994	2.85684287619035	1.83605034641159	1.60645695253936	0.922957439911236	1.70452405289955	0.176714560141169	-1.00330641528209	1.65032968302253	1.40336049531559	-1.78618243847232];
bestchrom=bestX;
net=newff(x_train_regular,y_train_regular,hidden_num,{'logsig','purelin'},'trainlm');
w1=bestchrom(1:input_num*hidden_num);   %输入和隐藏层之间的权重参数
B1=bestchrom(input_num*hidden_num+1:input_num*hidden_num+hidden_num); %隐藏层神经元的偏置
w2=bestchrom(input_num*hidden_num+hidden_num+1:input_num*hidden_num+...
    hidden_num+hidden_num*output_num);  %隐藏层和输出层之间的偏置
B2=bestchrom(input_num*hidden_num+hidden_num+hidden_num*output_num+1:input_num*hidden_num+...
    hidden_num+hidden_num*output_num+output_num); %输出层神经元的偏置
%网络权值赋值
net.iw{1,1}=reshape(w1,hidden_num,input_num);
net.lw{2,1}=reshape(w2,output_num,hidden_num);
net.b{1}=reshape(B1,hidden_num,1);
net.b{2}=reshape(B2,output_num,1);
net.trainParam.epochs=200;          %最大迭代次数
net.trainParam.lr=0.1;                        %学习率
net.trainParam.goal=0.00001;
[net,~]=train(net,x_train_regular,y_train_regular);
%将输入数据归一化
x_test_regular = mapminmax('apply',x_test_data,x_train_maxmin);
%放入到网络输出数据
y_test_regular=sim(net,x_test_regular);
%将得到的数据反归一化得到预测数据
SSA_BP_predict=mapminmax('reverse',y_test_regular,y_train_maxmin);
errors_nn=sum(abs(SSA_BP_predict'-y_test_data)./(y_test_data))/length(y_test_data);
EcRMSE=sqrt(sum((errors_nn).^2)/length(errors_nn));
disp(EcRMSE)