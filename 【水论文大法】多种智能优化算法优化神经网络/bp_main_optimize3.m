%https://mp.weixin.qq.com/s/nNZ5T237FQXBkUnnGjmNkQ
clc;clear; close all;
load('abalone_data.mat')%鲍鱼数据
global input_num hidden_num output_num input_data output_data train_num test_num  x_train_mu y_train_mu x_train_sigma  y_train_sigma
%% 导入数据
%设置训练数据和测试数据
[m,n]=size(data);
train_num=round(0.8*m); %自变量 
test_num=m-train_num;
x_train_data=data(1:train_num,1:n-1);
y_train_data=data(1:train_num,n);
%测试数据
x_test_data=data(train_num+1:end,1:n-1);
y_test_data=data(train_num+1:end,n);
% x_train_data=x_train_data';
% y_train_data=y_train_data';
% x_test_data=x_test_data';
%% 标准化
[x_train_regular,x_train_mu,x_train_sigma] = zscore(x_train_data);
[y_train_regular,y_train_mu,y_train_sigma]= zscore(y_train_data);
x_train_regular=x_train_regular';
y_train_regular=y_train_regular';
input_data=x_train_regular;
output_data=y_train_regular;
input_num=size(x_train_regular,1); %输入特征个数
hidden_num=6;   %隐藏层神经元个数
output_num=size(y_train_regular,1); %输出特征个数

num_all=input_num*hidden_num+hidden_num+hidden_num*output_num+output_num;%网络总参数，只含一层隐藏层;
%%
%自变量的个数即为网络连接权重以及偏置
popmax=1.5;   %自变量即网络权重和偏置的上限
popmin=-1.5;  %自变量即网络权重和偏置的下限
SearchAgents_no=50; % Number of search agents  搜索麻雀数量
Max_iteration=300; % Maximum numbef of iterations 最大迭代次数
% Load details of the selected benchmark function
%%
fobj=@objfun;
Time_compare=[];      %算法的运行时间比较
Fival_compare=[];       %算法的最终目标比较
Fival_compare1=[];     %优化过后的初始参数经过反向传播的优化
Fival_compare2=[];
curve_compare=[];     %算法的过程函数比较
name_all=[];     %算法的名称记录
dim=num_all;
lb=popmax*ones(1,dim);
ub=popmin*ones(1,dim);
pop_num=SearchAgents_no;
Max_iter=Max_iteration;
%% 不进行优化，随机赋值
iter=1;
bestX=lb+(ub-lb).*rand(1,num_all);
ER_=Fun1(bestX,x_train_regular,y_train_regular,hidden_num,x_test_data,y_test_data);
ER_1=Fun2(bestX,x_train_regular,y_train_regular,hidden_num,x_test_data,y_test_data);
Fival_compare1=[Fival_compare1,ER_]; 
Fival_compare2=[Fival_compare2,ER_1]; 
name_all{1,iter}='ON-opti';
iter=iter+1;
%% 麻雀搜索算法
t1=clock;
[fMin_SSA,bestX_SSA,SSA_curve]=SSA1(pop_num,Max_iter,lb,ub,dim,fobj);    %麻雀搜索算法

ER_SSA=Fun1(bestX_SSA,x_train_regular,y_train_regular,hidden_num,x_test_data,y_test_data);
ER_SSA1=Fun2(bestX_SSA,x_train_regular,y_train_regular,hidden_num,x_test_data,y_test_data);
t2=clock;
time_SSA=(t2(end)+t2(end-1)*60+t2(end-2)*3600-t1(end)-t1(end-1)*60-t1(end-2)*3600);
Fival_compare=[Fival_compare,fMin_SSA];   
Fival_compare1=[Fival_compare1,ER_SSA]; 
Fival_compare2=[Fival_compare2,ER_SSA1]; 
Time_compare=[Time_compare,time_SSA(end)];
curve_compare=[curve_compare;SSA_curve];
name_all{1,iter}='SSA';
iter=iter+1;
%%
%改进鲸鱼优化算法
t1=clock;
[fMin_EWOA,bestX_EWOA,EWOA_curve]=E_WOA(pop_num,Max_iter,lb,ub,dim,fobj); 
ER_EWOA=Fun1(bestX_EWOA,x_train_regular,y_train_regular,hidden_num,x_test_data,y_test_data);
ER_EWOA1=Fun2(bestX_EWOA,x_train_regular,y_train_regular,hidden_num,x_test_data,y_test_data);
t2=clock;
time_EWOA=(t2(end)+t2(end-1)*60+t2(end-2)*3600-t1(end)-t1(end-1)*60-t1(end-2)*3600);
Fival_compare=[Fival_compare,fMin_EWOA];
Fival_compare1=[Fival_compare1,ER_EWOA]; 
Fival_compare2=[Fival_compare2,ER_EWOA1]; 
Time_compare=[Time_compare,time_EWOA(end)];
curve_compare=[curve_compare;EWOA_curve];
name_all{1,iter}='EWOA';
iter=iter+1;
%%
%正弦余弦优化算法 Sine Cosine Algorithm
t1=clock;
[fMin_SCA,bestX_SCA,SCA_curve]=SCA(pop_num,Max_iter,lb,ub,dim,fobj); 
ER_SCA=Fun1(bestX_SCA,x_train_regular,y_train_regular,hidden_num,x_test_data,y_test_data);
ER_SCA1=Fun2(bestX_SCA,x_train_regular,y_train_regular,hidden_num,x_test_data,y_test_data);
t2=clock;
time_SCA=(t2(end)+t2(end-1)*60+t2(end-2)*3600-t1(end)-t1(end-1)*60-t1(end-2)*3600);
Fival_compare=[Fival_compare,fMin_SCA];
Fival_compare1=[Fival_compare1,ER_SCA]; 
Fival_compare2=[Fival_compare2,ER_SCA1]; 
Time_compare=[Time_compare,time_SCA(end)];
curve_compare=[curve_compare;SCA_curve];
name_all{1,iter}='SCA';
iter=iter+1;
%%
%正弦余弦优化算法 Sine Cosine Algorithm
t1=clock;
[fMin_SCA,bestX_SCA,SCA_curve]=POA(pop_num,Max_iter,lb,ub,dim,fobj); 
ER_SCA=Fun1(bestX_SCA,x_train_regular,y_train_regular,hidden_num,x_test_data,y_test_data);
ER_SCA1=Fun2(bestX_SCA,x_train_regular,y_train_regular,hidden_num,x_test_data,y_test_data);
t2=clock;
time_SCA=(t2(end)+t2(end-1)*60+t2(end-2)*3600-t1(end)-t1(end-1)*60-t1(end-2)*3600);
Fival_compare=[Fival_compare,fMin_SCA];
Fival_compare1=[Fival_compare1,ER_SCA]; 
Fival_compare2=[Fival_compare2,ER_SCA1]; 
Time_compare=[Time_compare,time_SCA(end)];
curve_compare=[curve_compare;SCA_curve];
name_all{1,iter}='POA';
iter=iter+1;
%%
load('color_list.mat')
figure(3)
color=color_list(randperm(length(color_list)),:);
width=0.7; %柱状图宽度
for  i=1:length(Fival_compare1) 
   set(bar(i,Fival_compare1(i),width),'FaceColor',color(i,:),'EdgeColor',[0,0,0],'LineWidth',2)
   hold on
    
   %在柱状图 x,y 基础上 绘制误差 ,low为下误差，high为上误差，LineStyle 误差图样式，'Color' 误差图颜色  
   % 'LineWidth', 线宽,'CapSize',误差标注大小
%    errorbar(i, y(i), low(i), high(i), 'LineStyle', 'none', 'Color', color(i+3,:), 'LineWidth', 1.5,'CapSize',18);
end
ylabel('obj-value')
ylim([min(Fival_compare1)-0.01,max(Fival_compare1)+0.01]);
ax=gca;
ax.XTick = 1:1:length(Fival_compare1);
set(gca,'XTickLabel',name_all,"LineWidth",2);
set(gca,"FontSize",12,"LineWidth",2)
title('优化工具箱')
%%
load('color_list.mat')
figure(4)
color=color_list(randperm(length(color_list)),:);
width=0.7; %柱状图宽度
for  i=1:length(Fival_compare2) 
   set(bar(i,Fival_compare2(i),width),'FaceColor',color(i,:),'EdgeColor',[0,0,0],'LineWidth',2)
   hold on
    
   %在柱状图 x,y 基础上 绘制误差 ,low为下误差，high为上误差，LineStyle 误差图样式，'Color' 误差图颜色  
   % 'LineWidth', 线宽,'CapSize',误差标注大小
%    errorbar(i, y(i), low(i), high(i), 'LineStyle', 'none', 'Color', color(i+3,:), 'LineWidth', 1.5,'CapSize',18);
end
ylabel('obj-value')
ylim([min(Fival_compare2)-0.01,max(Fival_compare2)+0.01]);
ax=gca;
ax.XTick = 1:1:length(Fival_compare2);
set(gca,'XTickLabel',name_all,"LineWidth",2);
set(gca,"FontSize",12,"LineWidth",2)
title('自写网络')
%%
figure(5)
bar([Fival_compare1;Fival_compare2]')
ylabel('obj-value')
ylim([min(Fival_compare2)-0.01,max(Fival_compare2)+0.01]);
ax=gca;
ax.XTick = 1:1:length(Fival_compare2);
set(gca,'XTickLabel',name_all,"LineWidth",2);
set(gca,"FontSize",12,"LineWidth",2)
legend('工具箱','自写网络')
%%
%%
% function fitness_value =objfun(input_pop,input_num,hidden_num,output_num,input_data,output_data)
function fitness_value =objfun(input_pop)
global input_num hidden_num output_num input_data output_data
    w1=input_pop(1:input_num*hidden_num);   %输入和隐藏层之间的权重参数
    B1=input_pop(input_num*hidden_num+1:input_num*hidden_num+hidden_num); %隐藏层神经元的偏置
    w2=input_pop(input_num*hidden_num+hidden_num+1:input_num*hidden_num+...
        hidden_num+hidden_num*output_num);  %隐藏层和输出层之间的偏置
    B2=input_pop(input_num*hidden_num+hidden_num+hidden_num*output_num+1:input_num*hidden_num+...
        hidden_num+hidden_num*output_num+output_num); %输出层神经元的偏置
    %网络权值赋值
    W1=reshape(w1,hidden_num,input_num);
    W2=reshape(w2,output_num,hidden_num);
    B1=reshape(B1,hidden_num,1);
    B2=reshape(B2,output_num,1);
    [~,n]=size(input_data);
    A1=logsig(W1*input_data+repmat(B1,1,n));   %需与main函数中激活函数相同
    A2=purelin(W2*A1+repmat(B2,1,n));      %需与main函数中激活函数相同  
    error=sumsqr(output_data-A2);
    fitness_value=error; %误差即为适应度
end
%%
function [EcRMSE]=Fun1(bestX,x_train_regular,y_train_regular,hidden_num,x_test_data,y_test_data)
global input_num output_num x_train_mu y_train_mu x_train_sigma  y_train_sigma
bestchrom=bestX;
net=newff(x_train_regular,y_train_regular,hidden_num,{'logsig','purelin'},'trainlm');
w1=bestchrom(1:input_num*hidden_num);   %输入和隐藏层之间的权重参数
B1=bestchrom(input_num*hidden_num+1:input_num*hidden_num+hidden_num); %隐藏层神经元的偏置
w2=bestchrom(input_num*hidden_num+hidden_num+1:input_num*hidden_num+...
    hidden_num+hidden_num*output_num);  %隐藏层和输出层之间的偏置
B2=bestchrom(input_num*hidden_num+hidden_num+hidden_num*output_num+1:input_num*hidden_num+...
    hidden_num+hidden_num*output_num+output_num); %输出层神经元的偏置
%网络权值赋值
net.iw{1,1}=reshape(w1,hidden_num,input_num);
net.lw{2,1}=reshape(w2,output_num,hidden_num);
net.b{1}=reshape(B1,hidden_num,1);
net.b{2}=reshape(B2,output_num,1);
net.trainParam.epochs=200;          %最大迭代次数
net.trainParam.lr=0.1;                        %学习率
net.trainParam.goal=0.00001;
[net,~]=train(net,x_train_regular,y_train_regular);
%将输入数据归一化
test_num=size(x_test_data,1);
x_test_regular = (x_test_data-repmat(x_train_mu,test_num,1))./repmat(x_train_sigma,test_num,1);
%放入到网络输出数据
y_test_regular=sim(net,x_test_regular');
%将得到的数据反归一化得到预测数据
 test_out_std=y_test_regular;
%反归一化
SSA_BP_predict=test_out_std*y_train_sigma+y_train_mu;
errors_nn=sum(abs(SSA_BP_predict'-y_test_data)./(y_test_data))/length(y_test_data);
EcRMSE=sqrt(sum((errors_nn).^2)/length(errors_nn));
disp(EcRMSE)
end
%%
function [EcRMSE]=Fun2(bestX,x_train_regular,y_train_regular,hidden_num,x_test_data,y_test_data)
global input_num output_num  x_train_mu y_train_mu x_train_sigma  y_train_sigma

train_num=length(y_train_regular); %自变量 
test_num=length(y_test_data);
bestchrom=bestX;
% net=newff(x_train_regular,y_train_regular,hidden_num,{'tansig','purelin'},'trainlm');
w1=bestchrom(1:input_num*hidden_num);   %输入和隐藏层之间的权重参数
B1=bestchrom(input_num*hidden_num+1:input_num*hidden_num+hidden_num); %隐藏层神经元的偏置
w2=bestchrom(input_num*hidden_num+hidden_num+1:input_num*hidden_num+...
    hidden_num+hidden_num*output_num);  %隐藏层和输出层之间的偏置
B2=bestchrom(input_num*hidden_num+hidden_num+hidden_num*output_num+1:input_num*hidden_num+...
    hidden_num+hidden_num*output_num+output_num); %输出层神经元的偏置
%网络权值赋值
x_train_std=x_train_regular;
y_train_std=y_train_regular;
%
vij = reshape(w1,hidden_num,input_num) ;%输入和隐藏层的权重
theta_u = reshape(B1,hidden_num,1);%输入与隐藏层之间的阈值
Wj =  reshape(w2,output_num,hidden_num);%%输出和隐藏层的权重
theta_y =reshape(B2,output_num,1);%输出与隐藏层之间的阈值
%
learn_rate = 0.0001;%学习率
Epochs_max = 10000;%最大迭代次数
error_rate = 0.1;%目标误差
Obj_save = zeros(1,Epochs_max);%损失函数
% 训练网络
epoch_num=0;
while epoch_num <Epochs_max
    epoch_num=epoch_num+1;
  
    y_pre_std_u=vij * x_train_std + repmat(theta_u, 1, train_num);
    y_pre_std_u1 = logsig(y_pre_std_u);
  
    y_pre_std_y = Wj * y_pre_std_u1 + repmat(theta_y, 1, train_num);
  
    y_pre_std_y1=y_pre_std_y;
    obj =  y_pre_std_y1-y_train_std ;
    
    Ems = sumsqr(obj);
    Obj_save(epoch_num) = Ems;
 
    if Ems < error_rate
        break;
    end
 
    %梯度下降
    %输出采用rule函数，隐藏层采用sigomd激活函数
    c_wj= 2*(y_pre_std_y1-y_train_std)* y_pre_std_u1';
    
    c_theta_y=2*(y_pre_std_y1-y_train_std)*ones(train_num, 1);
    
    c_vij=Wj'* 2*(y_pre_std_y1-y_train_std).*(y_pre_std_u1).*(1-y_pre_std_u1)* x_train_std';
    
    c_theta_u=Wj'* 2*(y_pre_std_y1-y_train_std).*(y_pre_std_u1).*(1-y_pre_std_u1)* ones(train_num, 1);
    
    Wj=Wj-learn_rate*c_wj;
    
    theta_y=theta_y-learn_rate*c_theta_y;
    
    vij=vij- learn_rate*c_vij; 
    
    theta_u=theta_u-learn_rate*c_theta_u;
end
 %
 x_test_regular = (x_test_data-repmat(x_train_mu,test_num,1))./repmat(x_train_sigma,test_num,1);
% x_test_regular = mapminmax('apply',x_test_data,x_train_maxmin);
%放入到网络输出数据
x_test_std=x_test_regular';
test_put = logsig(vij * x_test_std + repmat(theta_u, 1, test_num));
test_out_std =  Wj * test_put + repmat(theta_y, 1, test_num);
%反归一化
SSA_BP_predict=test_out_std*y_train_sigma+y_train_mu;
errors_nn=sum(abs(SSA_BP_predict'-y_test_data)./(y_test_data))/length(y_test_data);
EcRMSE=sqrt(sum((errors_nn).^2)/length(errors_nn));
disp(EcRMSE)
end