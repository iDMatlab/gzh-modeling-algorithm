%https://mp.weixin.qq.com/s/2Yv2gNIq1KM3CgOnCJepEA
clear;clear;close all
%% 求解程序
y=[108.68 ;118.71;120.13;106.71;88.5;95.15;98.85;89.11;74.33;77.29];%观测y值
x=(1:2:2*length(y))';n=length(x);%观测x的值
x_test= linspace(1,20,100)';m=length(x_test);%需要预测的自变量
%% 超参数优化
lb=[0;0];ub=[1000;10];can_0=[1;1];%参数优化上限限以及初值
[can,fval]=fmincon(@siran,can_0,[],[],[],[],lb,ub,[]);
sig_n=0.3;
theta0=can(1);
theta1=can(2);
Kff=gp_kernel(x,x,sig_n,theta0,theta1);
Kyy=gp_kernel(x_test,x_test,sig_n,theta0,theta1);
Kfy=gp_kernel(x,x_test,sig_n,theta0,theta1);
y_test=Kfy'*(Kff^-1)*y;
sigma_test=Kyy-Kfy'*(Kff^-1)*Kfy;
uncertainty=1.96*sqrt(diag(sigma_test));
un_low=y_test-uncertainty;
un_high=y_test+uncertainty;
%% 画图
h1=fill([x_test',fliplr(x_test')],[un_low',fliplr(un_high')],'r','DisplayName','uncertain');
hold on
COLOR=[214	237	254]/255;
COLOR1=[128	199	252]/255;
COLOR2=[190	210	254]/255;
COLOR3=[184	184	246]/255;
h1.FaceColor = COLOR;%定义区间的填充颜色      
h1.EdgeColor =[1,1,1];%边界颜色设置为白色
alpha .6   %设置透明色
plot(x_test,y_test,'Color',COLOR1,'LineWidth',1,'DisplayName','prediction') 
plot(x_test,un_low,'--','Color',COLOR2,'DisplayName','Lower  Limit')
plot(x_test,un_high,'--','Color',COLOR3,'DisplayName','Upper  Limit')
hold on
plot(x,y,'+','DisplayName','True Data');
legend('show','Location','Best');
%%
function K=gp_kernel(x1,x2,sig_n,theta0,theta1)
%核函数
m=size(x1,1);n=size(x2,1);
dist_matix=zeros(m,n);%距离矩阵
for i=1:m
    for j=1:n
        dist_matix(i,j)=sum((x1(i)-x2(j))^2);    
        if (i==j)
        K(i,j)=theta0^2*exp(-dist_matix(i,j)/2/(theta1^2))+sig_n^2;
        else
        K(i,j)=theta0^2*exp(-dist_matix(i,j)/2/(theta1^2));    
        end
    end
end
end
function object=siran(can)
%最大似然估计函数
y=[108.68 ;118.71;120.13;106.71;88.5;95.15;98.85;89.11;74.33;77.29];
x=(1:2:2*length(y))';
n=length(x);
sig_n=0.3;
K=gp_kernel(x,x,sig_n,can(1),can(2));
object=0.5*y'*(K^-1)*y+0.5*log(det(K))+n/2*log(2*pi);
end