%https://mp.weixin.qq.com/s/2Yv2gNIq1KM3CgOnCJepEA
clc;clear;
y=[108.68 ;118.71;120.13;106.71;88.5;95.15;98.85;89.11;74.33;77.29];
x=(1:2:2*length(y))';
%%  fitgp matlab自带的高斯拟合
gpr = fitrgp(x,y,'Sigma',0.05);
x_test = linspace(1,20,100)';
[y_test,~,limit] = predict(gpr,x_test);
%% 画图
h1=fill([x_test',fliplr(x_test')],[limit(:,1)',fliplr(limit(:,2)')],'r','DisplayName','uncertain');
hold on
COLOR=[184	184	246]/255;
COLOR1=[207	155	255]/255;
COLOR2=[190	210	254]/255;
COLOR3=[184	184	246]/255;
h1.FaceColor = COLOR;%定义区间的填充颜色      
h1.EdgeColor =[1,1,1];%边界颜色设置为白色
alpha .2   %设置透明色
plot(x_test,y_test,'Color',COLOR1,'LineWidth',1,'DisplayName','prediction') 
hold on
plot(x_test,limit(:,1),'--','Color',COLOR2,'DisplayName','Lower  Limit')
hold on
plot(x_test,limit(:,2),'--','Color',COLOR3,'DisplayName','Upper  Limit')
hold on
plot(x,y,'+','DisplayName','True Data');
legend('show','Location','Best');