%https://mp.weixin.qq.com/s/RqoDvMSonMpWvrWiKr01jg
clc;clear;close all;
load('abalone_data.mat')
n=size(data,2);
x=data(:,1:n-1); %自变量 
y=data(:,n);%因变量
x1=x;%扩展变量
X=ones(size(x,1),1);
X=[X,x];
alpha=0.05;%置信区间
[b,bint,r,rint,stats] = regress(y,X,alpha);
%alpha:显著性水平，默认时为0. 05。
%b:回归系数的最小二乘估计值。
%bint:回归系数的区间估计。
%r:模型拟合残差。
%rint:残差的置信区间。
%stats:用于检验回归模型的统计量。有四个数值:可决系数R^{2}、方差分析F统计量的值
%方差分析的显著性概率p的值以及模型方差估计值（剩余方差)。
y_n1=b(1)+X(:,2:end)*b(2:end);
wucha=sum(abs(y_n1-y)./y)/length(y);
figure(1)
color=[111,168,86;128,199,252;112,138,248;184,84,246]/255;
plot(y(3000:end),'Color',color(2,:),'LineWidth',1)
hold on
plot(y_n1(3000:end),'*','Color',color(1,:))
hold on
legend('真实数据','多元线性回归拟合数据')
disp('多元线性回归系数为：')
disp(b')
disp('平均相对误差为：')
disp(wucha)
fprintf('可决系数R^2为：%4.2f\n显著性概率p的值为：%4.2f\n',stats(1),stats(3)); 
title_str=['平均相对误差为：',num2str(wucha)];
title(title_str)