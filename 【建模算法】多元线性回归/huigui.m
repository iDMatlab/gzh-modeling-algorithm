%【建模算法】多元线性回归
%https://mp.weixin.qq.com/s/RqoDvMSonMpWvrWiKr01jg
clc;clear;close all;
load('abalone_data.mat')
n=size(data,2);
x=data(:,1:n-1); %自变量 
y=data(:,n);%因变量
%原自变量的多元线性回归
x_o=ones(size(x,1),1);
x_o=[x_o,x];
xishu1=(x_o'*x_o)^(-1)*x_o'*y;%原因变量系数
y_n1=xishu1(1)+x_o(:,2:end)*xishu1(2:end);
wucha=sum(abs(y_n1-y)./y)/length(y);
figure(1)
color1=[184	184	246]/255;
color2=[255	193	151]/255;
% color1=[111	168	86]/255;
% color2=[128	199	252]/255;
plot(y(3000:end),'Color',color1,'LineWidth',1)
hold on
plot(y_n1(3000:end),'*','Color',color2)
hold on
legend('真实数据','多元线性回归拟合数据')
disp('多元线性回归系数为：')
disp(xishu1')
disp('平均相对误差为：')
disp(wucha)
%%