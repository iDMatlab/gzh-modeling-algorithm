%https://mp.weixin.qq.com/s/GwoJoPITpnKJ773KetpoAA
clc;clear;close all
%原数据
data=[72.03,241.2,1592.74;73.84	,241.2,1855.36;74.49,244.8,2129.60;...
76.68,250.9,2486.86;78.00,250.9,2728.94;79.68,252.2,3038.90];
%要预测数据的真实值
data_T=[81.21,256.5,3458.05;82.84,259.4,3900.27;84.5,262.4,4399.06;...
86.19,265.3,4961.62;87.92,268.3,5596.1;89.69,271.4,	6311.79;91.49,274.5,7118.96];
%% 累加数据
data1=cumsum(data,1);
%% 求解系数
%对这三列分别进行预测
X=(1997:2002);%已知年份数据
X_p=(2003:2009);%预测年份数据
X_sta=X(1)-1;%最开始参考数据
for j=1:size(data,2)
    B=zeros(size(data,1)-1,2);
    for i=1:size(data,1)-1
        B(i,1)=-1/2*(data1(i,j)+data1(i+1,j));
        B(i,2)=1;
    end
    Y=data(2:end,j);
    a_u=inv(B'*B)*B'*Y;
    a=a_u(1); u=a_u(2);
    T=[X,X_p];
    data_p=(data1(1,j)-u/a).*exp(-a.*(T-X_sta))+u/a;%累加数据
    data_p1(1)=data_p(1);
    for n=2:length(data_p)
        data_p1(n)=data_p(n)-data_p(n-1);
    end
    title_str={'第一产业GDP预测','居民消费价格指数预测','第三产业GDP预测'};
    figure(1)
    subplot(2,2,j)
    plot(X,data(:,j),'*','LineWidth',1,'DisplayName','实际原数据')
    hold on 
    plot(X_p,data_T(:,j),'o','LineWidth',1,'DisplayName','预测参考数据')
    hold on
    data_n=data_p1;
    plot(X(2:end),data_n(X(2:end)-X_sta),'LineWidth',2,'DisplayName','拟合数据')
    plot(X_p,data_n(X_p-X_sta),'LineWidth',2,'DisplayName','预测数据')
    y_n=data_n(X_p-X_sta)';
    y=data_T(:,j);
    wucha=sum(abs(y_n-y)./y)/length(y);
    titlestr1=[title_str{1,j},'  预测相对误差：',num2str(wucha)];
    hold on
    legend('show','Location','Best');
    title(titlestr1)
end