%https://mp.weixin.qq.com/s/99_XYB4X-aMe_A0GQIaryw
clc;clear;close all;
year=1995:2006;
data=[20019.3,22913.5,24941.1,28406.2,29854.7,32917.7,37213.5,43499.9...
    55566.6,70477.4,88604.3,109869.8];%原始数据
d_2007=137239;%2007年的真实数据
a_list=(0:0.1:1);%平滑指数的取值
for n=1:length(a_list)
    a=a_list(n);%平滑指数，范围为（0-1）
    X0=data(1);   S0=X0;   %初始值
    X1=data(2:end);
    S1(1)=a*X1(1)+(1-a)*S0;
    for t=1:length(X1)-1
        S1(t+1)=a*X1(t+1)+(1-a)*S1(t);
    end
    S=[S0,S1];
    SS(:,n)=S;
    MSE(n)=sum((X1-S(1:length(X1))).^2)./length(X1);%平均相对值误差
end
subplot(2,1,1)
plot(1:length(a_list),MSE,'--*','LineWidth',2)
xlabel('平滑指数取值')
ylabel('均方根误差')
title('均方根误差随平滑指数变化图')
set(gca,'XTick',1:11)
set(gca,'XtickLabel',0:0.1:1)
subplot(2,1,2)
min_index=find(MSE==min(MSE));%找到使误差最小的平滑指数位置
a=a_list(min_index);
S=SS(:,min_index)';
plot(year,data,'--o','LineWidth',1.5)
hold on
plot(2007,d_2007,'o','LineWidth',1.5)
xlabel('year')
ylabel('投资额')
hold on
plot(year+1,S,'--x','LineWidth',1.5)
hold on
legend('真实数据','2007真实数据','预测数据')
title_str=['一次指数平滑预测','  平滑指数取值为：',num2str(a_list(min_index))];
title(title_str)