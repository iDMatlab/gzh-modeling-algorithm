%https://mp.weixin.qq.com/s/99_XYB4X-aMe_A0GQIaryw
clc;clear;close all;
year=1995:2006;
data=[20019.3,22913.5,24941.1,28406.2,29854.7,32917.7,37213.5,43499.9...
    55566.6,70477.4,88604.3,109869.8];%原始数据
d_2007=137239;%2007年的真实数据
N_list=[2,3,4,5];%步长数
for n=1:length(N_list)
    subplot(2,2,n)
    plot(year,data,'--o','LineWidth',1.5)
    hold on
    plot(2007,d_2007,'o','LineWidth',1.5)
    xlabel('year')
    ylabel('投资额')
    hold on
    N=N_list(n);    %设定步长数
    for t=N:length(year)
        M1(t)=sum(data(t-N+1:t))/N;%一次移动平均值
    end
    for t=(2*N-1):length(M1)
        M2(t)=sum(M1(t-N+1:t))/N;%二次移动平均值
    end
    %以2006年为当期数，预测2007年和2008年的投资额
    T=1:2;  %预预测时间长度x
    a=2*M1(end)-M2(end);
    b=2*(M1(end)-M2(end))/(N-1);
    y_p=a+b*T; %预测得到的2007年和2008年值
    T_p=year(end)+T;
    plot(T_p,y_p,'*','LineWidth',1.5)
    hold on
    %拟合2000-2005值
    T=1;  %预预测时间长度x
    a=2*M1(2*N-1:end-1)-M2(2*N-1:end-1);
    b=2*(M1(2*N-1:end-1)-M2(2*N-1:end-1))/(N-1);
    y_p2=a+b*T;
    plot(year((2*N-1:end-1)+1),y_p2,'--x','LineWidth',1.5)
    hold on
    legend('真实数据','2007真实数据','预测数据','拟合数据')
    title_str=['移动平均值预测法','  步长为：',num2str(N)];
    title(title_str)
end