%https://mp.weixin.qq.com/s/3uO_5GSAs9ucKsGTmyNlZg
clc;clear;close all;
load('abalone_data.mat')
data_get=data;
num_train = ceil(0.8*size(data_get,1));%共500个样本
%构造随机选择序列
choose = randperm(length(data));
%构建训练数据
train_data = data(choose(1:num_train),1:end);
test_data = data(choose(num_train+1:end),1:end);
%最后一列为分类标签
label_train = train_data(:,end);
label_test = test_data(:,end);
% 1. 随机产生训练集/测试集
P_train = train_data;
P_test = test_data;
T_train = label_train;
T_test =label_test ;
ntree = 200;
%% III. 创建随机森林分类器
Model = TreeBagger(ntree,train_data(:,1:end-1),label_train,'Method','regression','Surrogate','on',...
    'PredictorSelection','curvature','OOBPredictorImportance','on');
%% IV. 仿真测试
predict_label= predict(Model, test_data(:,1:end-1));
errors_nn=sum(abs(predict_label-T_test)./T_test/length(T_test));
figure(1)
color=[111,168,86;128,199,252;112,138,248;184,84,246]/255;
plot(T_test,'Color',color(2,:),'LineWidth',1)
hold on
plot(predict_label,'*','Color',color(1,:))
hold on
titlestr=['随机森林预测','   误差为：',num2str(errors_nn)];
title(titlestr)
%% 看分类树的情况
view(Model.Trees{1},'Mode','graph')
%% 重要性函数
figure(3)
imp = Model.OOBPermutedPredictorDeltaError;
figure;
bar(imp);
title('Curvature Test');
ylabel('重要性估计');
xlabel('特征');
h = gca;
h.XTickLabelRotation = 45;
h.TickLabelInterpreter = 'none';
%% 随机森林随树的棵树变化曲线
figure;
oobErrorBaggedEnsemble = oobError(Model);
plot(oobErrorBaggedEnsemble)
xlabel '树的数量';
ylabel '回归拟合误差';
%% 分位数预测
figure;
num_duan=200:300;  %数据太多可以单看某一个区间
mpgQuartiles = quantilePredict(Model,test_data(:,1:end-1),'Quantile',[0.5,0.75]); %选择0.5,0.7分位数预测值
plot(T_test(num_duan),'*','Color',color(2,:),'LineWidth',1)
hold on
plot(predict_label(num_duan),'Color',color(1,:),'LineWidth',1.5)
hold on
plot(mpgQuartiles(num_duan,:),'LineWidth',1);
hold on
ylabel('鲍鱼年龄');
xlabel('样本数据');
legend('真实值','平均预测值','第一分位数预测值','第二分位数预测值');