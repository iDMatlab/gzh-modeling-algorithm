%https://mp.weixin.qq.com/s/NS83kb8Il8ttya9-i2QTWQ
clc;clear;close all;
load('abalone_data.mat')%鲍鱼数据
%设置训练数据和测试数据
[m,n]=size(data);
train_num=round(0.8*m); %自变量 
x_train=data(1:train_num,1:n-1);
y_train=data(1:train_num,n);
%测试数据
x_test=data(train_num+1:end,1:n-1);
y_test=data(train_num+1:end,n);

%% 初始化参数
train_num =round(0.8*m); %输入样本数量
test_num = m-round(0.8*m);%测试样本数量
input_num = n-1;%输入变量数量
output_num = 1;%输出变量数量
[x_train_std,x_train_mu,x_train_sigma] = zscore(x_train);
[y_train_std,y_train_mu,y_train_sigma]= zscore(y_train);
%Z-score标准化，以训练集参数标准化测试集
x_test_std = (x_test-repmat(x_train_mu,test_num,1))./repmat(x_train_sigma,test_num,1);
x_train_std=x_train_std';
y_train_std=y_train_std';
x_test_std=x_test_std';
%%
layer_num=3; %隐藏层数+输出层
Neurons_num=[6,4]; %隐藏神经元
method=[1,2,3];  %激活函数的选择%1代表sigomd,2tanh,3puring
Neurons_num_all=[input_num,Neurons_num,output_num];
can_cell=cell(2,layer_num);% 记录网络参数的元胞
for i=1:layer_num
  can_cell{1,i}=2*rand(Neurons_num_all(i+1), Neurons_num_all(i))-1;
  can_cell{2,i}=2*rand(Neurons_num_all(i+1), 1)-1;
end
change_cell=cell(2,layer_num);%记录梯度的元胞

learn_rate = 0.0001;%学习率
Epochs_max = 10000;%最大迭代次数
error_rate = 1;%目标误差

%% 训练网络
epoch_num=0;
cell_output=cell(2,layer_num);
while epoch_num <Epochs_max
    epoch_num=epoch_num+1;
   cell_output{1,1}=can_cell{1,1} * x_train_std + repmat(can_cell{2,1}, 1, train_num);
   cell_output{2,1}=activation(cell_output{1,1},method(1));
   for i=2:layer_num
   cell_output{1,i}=can_cell{1,i} * cell_output{2,i-1}+ repmat(can_cell{2,i}, 1, train_num);
   cell_output{2,i}=activation(cell_output{1,i},method(i));
   end
    y_pre_std_y1=cell_output{2,layer_num};
    obj =  y_pre_std_y1-y_train_std ;
    
    Ems = sumsqr(obj);
    Obj_save(epoch_num) = Ems;
 
    if Ems < error_rate
        break;
    end
    if (layer_num>=3)   
        change_cell{2,layer_num}=2*(cell_output{2,layer_num}-y_train_std).*d_activation(cell_output{2,layer_num},method(layer_num));
        change_cell{1,layer_num}=2*(cell_output{2,layer_num}-y_train_std).*d_activation(cell_output{2,layer_num},method(layer_num))*(cell_output{2,layer_num-1})';
        for j=layer_num-1:-1:2
            change_cell{2,j}=(can_cell{1,j+1})'*change_cell{2,j+1}.*d_activation(cell_output{2,j},method(j));
            change_cell{1,j}=change_cell{2,j}*(cell_output{2,j-1})'; 
        end

        change_cell{2,1}=(can_cell{1,1+1})'*change_cell{2,1+1}.*d_activation(cell_output{2,1},method(1));
        change_cell{1,1}=change_cell{2,1}*x_train_std';  
    else
          change_cell{2,layer_num}=2*(cell_output{2,layer_num}-y_train_std).*d_activation(cell_output{2,layer_num},method(layer_num));
          change_cell{1,layer_num}=2*(cell_output{2,layer_num}-y_train_std).*d_activation(cell_output{2,layer_num},method(layer_num))*(cell_output{2,layer_num-1})';
          change_cell{2,1}=(can_cell{1,1+1})'*change_cell{2,1+1}.*d_activation(cell_output{2,1},method(1));
          change_cell{1,1}=change_cell{2,1}*x_train_std';  
    end
    %梯度下降
    for NN=1:layer_num
        can_cell{1,NN}=can_cell{1,NN}- learn_rate*change_cell{1,NN};
        can_cell{2,NN}=can_cell{2,NN}- learn_rate* change_cell{2,NN}*ones(train_num, 1);
    end
end
plot(Obj_save)
%%
cell_test_output{1,1}=can_cell{1,1} * x_test_std + repmat(can_cell{2,1}, 1, test_num);
cell_test_output{2,1}=activation(cell_test_output{1,1},method(1));
for i=2:layer_num
cell_test_output{1,i}=can_cell{1,i} * cell_test_output{2,i-1}+ repmat(can_cell{2,i}, 1, test_num);
cell_test_output{2,i}=activation(cell_test_output{1,i},method(i));
end
test_out_std=cell_test_output{2,i};
%反归一化
test_pre_out=test_out_std*y_train_sigma+y_train_mu;
errors_nn=sum(abs((test_pre_out'-y_test)./y_test))/length(y_test);
%% 画图
figure(1)
plot(Obj_save,'b-','LineWidth',1.5);
title('损失函数')
xlabel('epoch')
ylabel('errors')
figure(2)
color=[111,168,86;128,199,252;112,138,248;184,84,246]/255;
plot(y_test,'Color',color(2,:),'LineWidth',1)
hold on
plot(test_pre_out,'*','Color',color(1,:))
hold on
titlestr=['公式推导多层BP神经网络','   误差为：',num2str(errors_nn)];
title(titlestr)
function y=activation(x,method) %,1为sigmod ,2为tanh ,3为purling
 %激活函数
  if method==1
        y=logsig(x);      
  elseif method==2
       y=tanh(x);   
  elseif method==3
          y=x;
  end
end

function y=d_activation(x,method) %,1为sigmod ,2为tanh ,3为purling
 %激活函数
 [m,n]=size(x);
  if method==1
        y=(x).*(1-x);      
  elseif method==2
       y=1-(x).^2;   
  elseif method==3
          y=ones(m,n);
  end
end