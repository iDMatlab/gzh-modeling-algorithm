%https://mp.weixin.qq.com/s/y6JpdgQly6gfN7gZYeqK7Q
clc;clear;close all;
load('iri_data.mat')
%%
data=iri_data;
train_num=round(0.8*size(data,1));%取整个数据0.8的比例训练，其余作为测试数据
choose=randperm(size(data,1));
%%
train_data=data(choose(1:train_num),:);
test_data=data(choose(train_num+1:end),:);
n=size(data,2);
y=train_data(:,n);
x=train_data(:,1:n-1);
[x,PS]=mapminmax(x',0,1);
x=x';
y_t=test_data(:,n);
x_t=test_data(:,1:n-1);
x_t=mapminmax('apply',x_t',PS);
x_t=x_t';
n=3; %标签数量
Index_Pre=zeros(size(y_t,1),1);%记录预测的标签
distance_num=round(0.3*size(x,1));    %取数据0.3作为排序后取的数据
%%
for N=1:length(Index_Pre)
distance_juzheng=juli(x_t,x); 
[distance_sort,index_sort]=sort(distance_juzheng(N,:));
distance_sort_part=index_sort(1:distance_num);    %取前N个距离最小的数index
index_all=y(distance_sort_part);    %判断前N个最小距离分别属于哪类
index_ta=tabulate(index_all);
max_index=find(index_ta(:,2)==max(index_ta(:,2)));
index_pre=index_ta(max_index(1),1);
Index_Pre(N,1)=index_pre;
end
accuracy = length(find(Index_Pre==y_t))/length(y_t);
disp('手写KNN算法准确率')
disp(accuracy)
%% MATLAB自带KNN算法
mdl = fitcknn(x, y,'NumNeighbors',distance_num);%k为对应的1,2,3,4.....
Index_Pre1 = predict(mdl,x_t);
% Index_Pre1=knnclassify(x_t,x,y_t,3,'cosine','random');%2016版本的用法
accuracy1 = length(find(Index_Pre1==y_t))/length(y_t);
disp('MATLAB自带KNN算法准确率')
disp(accuracy1)
%% 距离计算矩阵
function distance_juzheng=juli(x,y)
distance_juzheng=zeros(size(x,1),size(y,1));
for i=1:size(x,1)
    for j=1:size(y,1)
        distance_juzheng(i,j)=sqrt(sum((x(i,:)-y(j,:)).^2));
    end
end
end