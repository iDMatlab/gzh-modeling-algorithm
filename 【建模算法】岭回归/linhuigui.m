%https://mp.weixin.qq.com/s/QOzLyPkBX8283a2_-ZAFdg
clc;clear;close all;
load('abalone_data.mat')
n=size(data,2);
x=data(:,1:n-1);y=data(:,n);
k = 1:0.1:10;
B = ridge(y,x,k,0);
%B = ridge(y,X,k,scaled)
%k为岭参数，scaled为标准化的范围
figure(1)
plot(k,B,'LineWidth',2)
legend('x1','x2','x3','x4','x5','x6','x7','x8')
%ylim([-100 100])
grid on 
xlabel('Ridge Parameter') 
ylabel('Standardized Coefficient') 
title('Ridge Trace') 
for  k1 = 1:length(k)
    A=B(:,k1);
    yn= A(1)+x*A(2:end);
    wucha(k1)=sum(abs(y-yn)./y)/length(y);
end
figure(2)
subplot(2,1,1)
plot(1:length(k),wucha,'LineWidth',2)
ylabel('相对误差')
xlabel('岭参数')
set(gca,'XTick',1:10:100)
set(gca,'XtickLabel',1:1:10)
subplot(2,1,2)
index=find(wucha==min(wucha));
xishu = ridge(y,x,k(index),0);
y_p= A(1)+x*A(2:end);
color1=[184	184	246]/255;
color2=[255	193	151]/255;
titlestr=['岭参数取值为：',num2str(k(index)),'   误差为：',num2str(min(wucha))];
plot(y(3000:end),'Color',color1,'LineWidth',1)
hold on
plot(y_p(3000:end),'*','Color',color2)
hold on
legend('真实数据','多元线性回归拟合数据')
title(titlestr)