clc;clear;close all;
load('abalone_data.mat')
n=size(data,2);
x=data(:,1:n-1); %自变量 
y=data(:,n);       %因变量
xm=mean(x);    %x的平均值
xs=std(x);         %x的方差
ym=mean(y); %y的平均值
%原自变量的多元线性回归
 x_o=zscore(x);
k1=[1:0.1:10];
for k=1:length(k1)
%用标准化后的进行多元线性回归
lamda=k1(k);
xishu=(x_o'*x_o+lamda*eye(size(x_o,2)))^(-1)*x_o'*(y-ym);
%还原为原自变量的系数
xishu1=[ym-sum(xishu.*xm'./xs');xishu./xs'];
y_n1=xishu1(1)+x*xishu1(2:end);
wucha(k)=sum(abs(y_n1-y)./y)/length(y);
end
plot(wucha,'LineWidth',2)
ylabel('相对误差')
xlabel('岭参数')
set(gca,'XTick',1:10:100)
set(gca,'XtickLabel',1:1:10)