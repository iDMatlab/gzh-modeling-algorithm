%https://mp.weixin.qq.com/s/vhbZc09Qgb6qRikSkmqP-w
clc;clear;close all;
data=[43.31,7.39,8.73,54.89,15.35;
17.11,12.13,17.29,44.25,29.69;
21.11,6.03,7,89.37,13.82;
29.55,8.62,10.13,73,14.88;
11,8.41,11.83,25.22,25.49;
17.63,13.86,15.41,36.44,10.03;
2.73,4.22,17.16,9.96,74.12];
data=reshape(data,[7,5]); 
m=size(data,1); 
x=data(:,5);
data=data(:,1:4);num=2; 
data=zscore(data); %数据标准化 
r=cov(data); 
[vec,val,con]=pcacov(r); %进行主成分分析的相关计算 
f1=repmat(sign(sum(vec)),size(vec,1),1); 
vec=vec.*f1; %特征向量正负号转换 
f2=repmat(sqrt(val)',size(vec,1),1); 
a=vec.*f2; %载荷矩阵 
%如果指标变量多，选取的主因子个数少，可以直接使用factoran进行因子分析 
%本题中4个指标变量，选取2个主因子，factoran无法实现 
[b,t]=rotatefactors(a(:,1:num),'method', 'varimax'); %旋转变换 
bz=[b,a(:,num+1:end)] ;%旋转后的载荷矩阵 
gx=sum(bz.^2); %计算因子贡献 
gxv=gx/sum(gx); %计算因子贡献率 
dfxsh=inv(r)*b; %计算得分函数的系数 
df=data*dfxsh ;%计算各个因子的得分 
zdf=df*gxv(1:num)'/sum(gxv(1:num)); %对各因子的得分进行加权求和 
[szdf,ind]=sort(zdf,'descend') ;%对企业进行排名 
xianshi=[df(ind,:)';zdf(ind)';ind']; %显示计算结果 
[x_zdf_coef,p]=corrcoef([zdf,x]) ;%计算相关系数 
[d1,d1int,d2,d2int,stats]=regress(zdf,[ones(m,1),x]) ;%回归分析计算
