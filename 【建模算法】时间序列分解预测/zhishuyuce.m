%https://mp.weixin.qq.com/s/Zwn3bNxj5-5d1sGyf6tYBA
clc;clear
load('data_load.mat')
data=data_get;
train_data=data(1:96*15);
test_data=data(96*15+1:end);
N=96;    %周期
for t=1:(length(train_data)-N+1)
    MA(t)=sum(train_data(t:(t+N-1)))/N;
end
SI=100*train_data((N-1):end-1)./MA;
%用各年同季平均，去掉SI的随机性，得到季节指数r，并修正季节指数R
N_list=[N-1:N,1:N-2];%SI是从N-1开始的，循环一个周期
for i=1:length([N-1:N,1:N-2])
    N1=N_list(i);
    r(i)=mean(SI(N1:N:end));
end
R=r./mean(r);
%拟合得到长期趋势T
x=1:length(train_data);
p=polyfit(x,train_data,2); %用二次方程拟合
T=polyval(p,x);%长期趋势
% 计算循环变动C
C=MA./T((N-1):end-1);
%预测后面一个季度的数据
N_p=2;%预测后两季度
T_n=length(train_data);
T_p=(T_n+1):(T_n+N_p*N);%预测后面两个周期
T_p1=polyval(p,T_p);  %长期趋势
C1=mean(C);
X_p=T_p1.*C1.*repmat(R,1,N_p);
subplot(2,1,1)
plot(train_data)
xlabel('time')
ylabel('power load')
hold on
plot(T,'--','LineWidth',1)%长期趋势
hold on
plot(T_p,test_data,'--','LineWidth',0.5)
hold on
plot(T_p,X_p,'--','LineWidth',1)
hold on
legend('真实值','长期趋势','后几个周期真实值','后几个周期预测值')
wucha=sum(abs(X_p-test_data)./test_data)./length(X_p);
title_str=['时间序列分解法','  后几个周期预测相对误差为：',num2str(wucha)];
title(title_str)
subplot(2,1,2)
plot(T_p,test_data,'--o','LineWidth',0.5)
hold on
plot(T_p,X_p,'--*','LineWidth',1)
hold on
xlabel('time')
ylabel('power load')
legend('后几个周期真实值','后几个周期预测值')
wucha=sum(abs(X_p-test_data)./test_data)./length(X_p);
title_str=['时间序列分解法','  后几个周期预测相对误差为：',num2str(wucha)];
title(title_str)