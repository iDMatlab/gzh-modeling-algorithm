%%https://mp.weixin.qq.com/s/Zwn3bNxj5-5d1sGyf6tYBA
clc;clear;close all;
data=[562 575 398 360 616 604 386 451 675 693 420 535 ...
    596 602 467 574 680 672 492 548 729 683 492 673 ...
    760 724 501 647 739 708 570 695 781 756 608 642 ...
    800 806 649 752 860 893 695 723 902 875 684 692];
train_data=data(1:4*11);
test_data=data(4*11+1:end);
train_data=train_data';
figure(1)
plot(train_data(:),'-o')
xlabel('季度')
ylabel('销量')
hold on
N=4;%一个周期的数据
% 计算一次移动平均值MA
for t=1:(length(train_data)-N+1)
    MA(t)=sum(train_data(t:(t+N-1)))/N;
end
%计算季节变动因素S和随机变动因素I
SI=100*train_data((N-1):end-1)./MA';
%用各年同季平均，去掉SI的随机性，得到季节指数r，并修正季节指数R
N_list=[N-1:N,1:N-2];%SI是从N-1开始的，循环一个周期
for i=1:length([N-1:N,1:N-2])
    N1=N_list(i);
    r(i)=mean(SI(N1:N:end));
end
R=r./mean(r);
%拟合得到长期趋势T
x=1:length(train_data);
p=polyfit(x,train_data,1);
T=polyval(p,x);%长期趋势
plot(T,'-*')
hold on
% 计算循环变动C
C=MA./T((N-1):end-1);
%预测后面一个季度的数据
T_n=length(train_data);
T_p=(T_n+1):(T_n+N);
T_p1=polyval(p,T_p);  %长期趋势
C1=mean(C);
X_p=T_p1.*C1.*R;
plot(T_p,test_data,'-o','LineWidth',1.5)
hold on
plot(T_p,X_p,'-*','LineWidth',1)
hold on
legend('真实值','长期趋势','后一季度真实','后一季度预测值')
wucha=sum(abs(X_p-test_data)./test_data)./length(X_p);
title_str=['时间序列分解法','  后一季度预测相对误差为：',num2str(wucha)];
title(title_str)