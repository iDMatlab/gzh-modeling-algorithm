%%https://mp.weixin.qq.com/s/zruYhAxxBxHTdFuj4ojiIg
clc;clear;close all;
data = [ 16.88 15.91 15.92 16.44 16.44 16.87 17.00 17.35 17.50 17.94...
18.52 18.62 19.39 17.45 17.48 16.28 16.49 15.65 15.80 16.35 16.91 16.57 16.42...
17.32 17.25 17.04 16.40 16.43 16.21 16.16 16.39 16.24 17.04 17.24 16.97 16.84 ...
17.05 17.41 17.48 18.03 18.95 18.80 18.76 18.29 18.10 18.61 18.83 19.18 19.00 ...
18.86 18.67 19.23 20.0 20.24 19.65 19.63];
train_data=data(1:4*11);
test_data=data(4*11+1:end);%测试数据，用于预测对比
%%  看数据是否平稳，不平稳进行差分处理
figure(1)
plot(train_data,'LineWidth',1)
pinwen=adftest(train_data); %1代表平稳，0代表不平稳
%非平稳进行差分处理
xlabel('day')
ylabel('price')
if pinwen==1
    disp('平稳序列')
    train_data1=train_data;
else
    disp('不平稳序列')
    figure(2)
    train_data1=diff(train_data);
    %train_data1=train_data;
    plot(train_data1,'LineWidth',1)
end
%% 利用自相关图和偏相关图判断模型类型和阶次
figure(3)
autocorr(train_data1) %绘制自相关函数
[ACF,Lags,Bounds]=autocorr(train_data);
figure(4)
parcorr(train_data1) %绘制偏相关函数
%% 自相关和偏相关函数难以判断时可以用AIC准则求出最好阶数
%确定阶数的上限
lim=round(length(train_data)/10); %数据总长度的1/10
if lim>=10
    lim=10;%如果数据太长了，就限定阶数
end
train_data2=iddata(train_data');
save_data=[];
for p=1:lim
    for q=1:lim
        num=armax(train_data2,[p,q]);  %armax对应FPE最小
        AIC=aic(num);
        save_data=[save_data;p q AIC];
        reli_juzheng(p,q)=AIC;
    end
end
%AIC越小越好
%% 绘制阶数热力图
figure(5)
for i=1:lim
    y_index(1,i)={['AR' ,num2str(i)]};
    x_index(1,i)={['MA' ,num2str(i)]};
end
H = heatmap(x_index,y_index, reli_juzheng, 'FontSize',12, 'FontName','宋体');
H.Title = 'AIC定阶热力图'; 
%AIC越小越好
figure(6)
%% 利用阶数得到模型
min_index=find(save_data(:,3)==min(save_data(:,3)));
p_best=save_data(min_index,1);  %p的最优阶数
q_best=save_data(min_index,2);  %q的最优阶数
model=armax(train_data2,[p_best,q_best])
%% 利用模型预测
L=length(test_data);
pre_data=[train_data1';zeros(L,1)];
pre_data1=iddata(pre_data);
pre_data2=predict(model,pre_data1,L);
pre_data3=get(pre_data2);%得到结构体
pre_data4=pre_data3.OutputData{1,1}(length(train_data1)+1:length(train_data1)+L);%从结构体里面得到数据
%显示全部
data=[train_data1';pre_data4];%全部的差分值
if pinwen==0 %非平稳时进行差分还原
   data_pre1=cumsum([train_data(1);data]);%还原差分值
elseif pinwen==1
    data_pre1=data;
end
data_pre2=data_pre1(length(train_data)+1:end);%最终预测值
subplot(2,1,1)
plot(1:length(train_data),train_data,'--','LineWidth',1)
hold on
plot(length(train_data)+1:length(train_data)+L,test_data,'--','LineWidth',1.5)
hold on
plot(length(train_data)+1:length(train_data)+L,data_pre2,'--','LineWidth',1.5)
hold on
xlabel('time')
ylabel('price')
legend('真实值','测试数据真实值','预测值')
wucha=sum(abs(data_pre2'-test_data)./test_data)./length(data_pre2);
title_str=['ARMA法','  预测相对误差为：',num2str(wucha)];
title(title_str)
subplot(2,1,2)
plot(1:L,test_data,'--o','LineWidth',1.5)
hold on
plot(1:L,data_pre2,'--*','LineWidth',1.5)
hold on
xlabel('time')
ylabel('price')
legend('真实值','预测值')
title_str=['ARMA法','  预测相对误差为：',num2str(wucha)];
title(title_str)