%https://mp.weixin.qq.com/s/YhpvkKkH3iXWVMecr6xYWg
clc
clear
%%
t=-4:0.05:4*pi;x=cos(t);y=sin(t);z=t;
scatter3(x,y,z,'g');hold on
data=[x;y;z]';
data=zscore(data);
R=corr(data);
%计算特征向量和特征值
[V,D] = eig(R);  %V特征向量，D特征值对角线矩阵
%%
lam=diag(D);%取出对角线元素
%对特征值从大到小排列
[lam_sort,index]=sort(lam,'descend');
V_sort=V(:,index);
gong=lam_sort./sum(lam_sort); %贡献率
cgong=cumsum(gong); %累计贡献率
index1=2; %找到累计贡献达到85%的位置
%%
M=data*V_sort;
M=M(:,1:index1);  %这就是得到的新的累计贡献率超过85%主成分
plot(M(:,1),M(:,2),'b')
%%
hold on
title('三维数据降为两维数据')
legend('降维前的三维数据','降维后的二维数据')
xlabel('x')
ylabel('y')
zlabel('z')