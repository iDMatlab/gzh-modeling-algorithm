%%https://mp.weixin.qq.com/s/HKgZ9uNsEA8rpYh0o2GJGw
clc;clear;close all;
data(:,1)=[57,67,90,51,50,5,19,89,17,93,10,85,65,12,54,92,11,38,30,86,15,48,22,7,68,44,...
    39,26,72,32,27,4,97,9,69,88,35,99,70,52,3,61,20,29,98,2,53,13,31,34];
data(:,2)=[29,23,52,13,61,2,27,4,17,85,90,12,64,3,28,79,41,36,87,9,40,...
    18,51,94,11,60,53,7,69,80,14,44,47,33,83,86,31,6,67,95,10,45,70,8,39,78,21,5,59,37];
data(:,3)=[27,31,58,23,40,52,94,1,41,56,4,7,13,84,95,70,46,51,20,86,92,30,78,38,10,43,88,...
    32,77,25,22,2,42,55,24,76,3,34,47,72,39,48,36,9,28,21,83,15,73,61];
figure(1)
scatter3(data(:,1),data(:,2),data(:,3),'LineWidth',2)
%% MATLAB自带Kmeans函数
cluster_num=4;
t3=clock;
[index_kmedoids,Cluster]=kmeans(data,cluster_num);
t4=clock;
index_cluster=index_kmedoids;
figure(2)
subplot(2,1,1)
a=unique(index_cluster); %找出分类出的个数
C=cell(1,length(a));
for i=1:length(a)
   C(1,i)={find(index_cluster==a(i))};
end
for j=1:cluster_num
    data_get=data(C{1,j},:);
    scatter3(data_get(:,1),data_get(:,2),data_get(:,3),100,'filled','MarkerFaceAlpha',.6,'MarkerEdgeAlpha',.9);
    hold on;
end
plot3(Cluster(:,1),Cluster(:,2),Cluster(:,3),'k*','LineWidth',3);
hold on
sc_t=mean(silhouette(data,index_cluster));
title_str=['MATLAB自带 kmeans函数','  聚类数为：',num2str(cluster_num),'  SC轮廓系数:',num2str(sc_t),' 运行时间:',num2str(t4(end)-t3(end))];
title(title_str)
%% MATLAB自带函数kmedoids
cluster_num=4;
t3=clock;
[index_kmedoids,Cluster]=kmedoids(data,cluster_num);
t4=clock;
index_cluster=index_kmedoids;
figure(2)
subplot(2,1,2)
a=unique(index_cluster); %找出分类出的个数
C=cell(1,length(a));
for i=1:length(a)
   C(1,i)={find(index_cluster==a(i))};
end
for j=1:cluster_num
    data_get=data(C{1,j},:);
    scatter3(data_get(:,1),data_get(:,2),data_get(:,3),100,'filled','MarkerFaceAlpha',.6,'MarkerEdgeAlpha',.9);
    hold on;
end
plot3(Cluster(:,1),Cluster(:,2),Cluster(:,3),'k*','LineWidth',3);
hold on
sc_t=mean(silhouette(data,index_cluster));
title_str=['MATLAB自带 kmedoids函数','  聚类数为：',num2str(cluster_num),'  SC轮廓系数:',num2str(sc_t),' 运行时间:',num2str(t4(end)-t3(end))];
title(title_str)