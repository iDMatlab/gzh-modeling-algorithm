%%https://mp.weixin.qq.com/s/HKgZ9uNsEA8rpYh0o2GJGw
clc;clear;close all;
data=xlsread('附件1.xlsx');
data=data(:,2:end);
xla=data(1,:);
data1=data(2:end,:);
for i=1:size(data1,1)
    plot(data1(i,:))
    hold on
end
%% MATLAB自带Kmeans函数
cluster_num=4;
t3=clock;
[index_kmeans,Cluster]=kmeans(data1,cluster_num);
t4=clock;
index_cluster=index_kmeans;
disp('自带kmeans函数运行时间')
disp(t4(end)-t3(end))
figure(2)
subplot(2,1,1)
a=unique(index_cluster); %找出分类出的个数
C=cell(1,length(a));
for i=1:length(a)
   C(1,i)={find(index_cluster==a(i))};
end
ma=max(max(data1));
for j=1:cluster_num
    x=xla;
    subplot(2,2,j)
    n1=length(C{1,j});
    for i=1:n1
        plot(x,data1(C{1,j}(i,1),:))
        hold on
        title(['kmeans聚类 Class label: ',num2str(j)])
    end
    xlabel('波数')
    ylabel('吸光度')
    axis([-inf,inf,0,ma])
end
sc_t=mean(silhouette(data1,index_cluster));
disp('kmeans-SC轮廓系数')
disp(sc_t)
%% MATLAB自带函数kmedoids
t3=clock;
figure(3)
[index_kmedoids,Cluster]=kmedoids(data1,cluster_num);
t4=clock;
index_cluster=index_kmedoids;
disp('自带kmedoids函数运行时间')
disp(t4(end)-t3(end))
subplot(2,1,2)
a=unique(index_cluster); %找出分类出的个数
C=cell(1,length(a));
for i=1:length(a)
   C(1,i)={find(index_cluster==a(i))};
end
ma=max(max(data1));
for j=1:cluster_num
    x=xla;
    subplot(2,2,j)
    n1=length(C{1,j});
    for i=1:n1
        plot(x,data1(C{1,j}(i,1),:))
        hold on
        title(['kmedoids函数 Class label: ',num2str(j)])
    end
    xlabel('波数')
    ylabel('吸光度')
    axis([-inf,inf,0,ma])
end
sc_t=mean(silhouette(data1,index_cluster));
disp('kmedoids-SC轮廓系数')
disp(sc_t)