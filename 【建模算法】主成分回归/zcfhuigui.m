%https://mp.weixin.qq.com/s/3BsUXNARCANn8WrY80XHdg
clc;clear;close all;
load('abalone_data.mat')
n1=size(data,2);
X=data(:,1:n1-1);Y=data(:,n1);
m_x=mean(X);  %X的平均值 
m_y=mean(Y);  %Y的平均值
std_x=std(X,0); %X的标准差
std_y=std(Y,0); %Y的标准差
data1=zscore(data); %z_score标准化
X1=data1(:,1:n1-1);
Y1=data1(:,n1);
R=corr(X1);%相关系数矩阵
%计算特征向量和特征值
[V,D] = eig(R);  %V特征向量，D特征值对角线矩阵
lam=diag(D);%取出对角线元素
%对特征值从大到小排列
[lam_sort,index]=sort(lam,'descend');
V_sort=V(:,index);
gong=lam_sort./sum(lam_sort); %贡献率
cgong=cumsum(gong); %累计贡献率
index1=[5,8]; %找到累计贡献比较高的位置
M=X1*V_sort;
for i=1:length(index1)
index=index1(i);
M1=M(:,1:index);  %得到的新成分
X1=[ones(size(data,1),1),M1];
xishu=(X1'*X1)^(-1)*X1'*Y1;    %主成分后的系数
xishu1=[xishu(1),[xishu(2)*V_sort(:,1)]'];
%% 记录标准化数据的主成分回归方程的系数
for j=3:index+1
    xishu1(2:end)=xishu1(2:end)+[xishu(j)*V_sort(:,j-1)]';
end
%% 还原为原始数据的回归方程系数
 xishu2=[((xishu1(1)-sum(xishu1(2:length(xishu1)).*m_x./std_x))*...
     std_y+m_y)/std_y,xishu1(2:length(xishu1))./std_x]*std_y;
 xishu2=xishu2';
 y_n1=xishu2(1)+X*xishu2(2:end);
wucha=sum(abs(y_n1-Y)./Y)/length(Y);
figure(1)
subplot(2,1,i)
color=[111,168,86;128,199,252;234,168,135;184,84,246]/255;
plot(Y(3000:length(Y)),'Color',color(i+1,:),'LineWidth',1)
hold on
plot(y_n1(3000:length(Y)),'*','Color',color(i,:))
hold on
titlestr=['取前',num2str(index),'主成分','  相对误差为：',num2str(wucha)];
legend('真实数据','主成分回归拟合数据')
title(titlestr)
end