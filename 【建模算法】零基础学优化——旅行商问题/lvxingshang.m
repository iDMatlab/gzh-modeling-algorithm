%https://mp.weixin.qq.com/s/f6eoh2NEd9jFkwSH-TL5Og
clc;clear;close all;
%点坐标
data_x=[565;25;345;945;845;880;25;525;580;650;1605;1220;1465;1530;845;725;...
    145;415;510;560;300;520;480;835;975;1215;1320;1250;660;410;420;575;1150;700;...
    685;685;770;795;720;760;475;95;875;700;555;830;1170;830;605;595;1340;1740];
%y点坐标
data_y=[575;185;750;685;655;660;230;1000;1175;1130;620;580;200;5;680;370;665;...
    635;875;365;465;585;415;625;580;245;315;400;180;250;555;665;1160;580;595;...
    610;610;645;635;650;960;260;920;500;815;485;65;610;625;360;725;245];
%% 显示一下坐标点
figure(1)
scatter(data_x,data_y,40)
%% 计算各个坐标点之间的距离
num=length(data_x); %城市点的个数
distance_juzheng=zeros(num,num);
for i =1:num
    for j=1:num
        distance_juzheng(i,j)=sqrt((data_x(i)-data_x(j))^2+(data_y(i)-data_y(j))^2);
    end
end
%%
%定义变量，x为0-1变量记录，表示
x=binvar(num,num,'full');
u=sdpvar(1,num);

%目标
obj=sum(sum(x.*distance_juzheng));

%约束条件
%1.每个城市只能经过一次 ，0-1矩阵每一行加起来只能为1，每一列加起来只能为1.
constr=[];  %循环添加约束条件
for i = 1:num
    sum_constr= sum(x(i,:)) - x(i,i);
    constr = [constr, sum_constr  == 1];
end
for j = 1:num
    sum_constr = sum(x(:,j))-x(j,j);
    constr = [constr,   sum_constr  == 1];
end
% 约束条件2，不允许出现子回路
for i = 2:num
    for j = 2:num
        if i~=j
            constr = [constr,u(i)-u(j) + num*x(i,j)<=num-1];
        end
    end
end
%优化
%%
optimize(constr,obj)
disp('自变量矩阵为')
disp(value(x))

disp('目标函数最优值')
disp(value(obj))

%% 画图展示
bianlian=value(x);
index_all=[];
for i=1:num
    if i==1
   index=find(bianlian(:,i)==1);
    else
    index=find(bianlian(:,index_all(i-1))==1);   
    end
index_all=[index_all,index];
end
index_all=[index_all,index_all(1)];

figure(2)
for n=1:num
plot(data_x(index_all(n:n+1)),data_y(index_all(n:n+1)),'--o','Color',[135,206,235]./255,'MarkerSize',8,'MarkerFaceColor',[160,102,211]./255,'LineWidth',2);
hold on 
end
titlestr=['最佳路径如下', '   最短距离为：',num2str(value(obj))];
title(titlestr)
xlabel('x坐标')
ylabel('y坐标')