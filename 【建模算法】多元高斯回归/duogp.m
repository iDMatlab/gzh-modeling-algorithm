%【建模算法】多元高斯回归
%https://mp.weixin.qq.com/s/Y_0K5XbAVwhPEe0rTzsXug
clc;clear;
load('abalone_data.mat')%将csv数据做成了mat数据
%%
n=size(data,2);
x=data(:,1:n-1);%自变量
y=data(:,n);%因变量
%%
gprMdl = fitrgp(x,y,'KernelFunction','ardsquaredexponential',...
      'FitMethod','sr','PredictMethod','fic','Standardize',1);%数据量大的时候拟合会比较慢
%% 'FitMethod': 模型参数估计的方法
% 'none' : 不估计，使用初始参数值作为已知参数值
% 'exact'：精确高斯过程回归。如果n ≤ 2000，则默认为此设置，其中n是观察数。
% 'sd' : 数据点近似的子集。如果n > 2000，则默认为此设置，其中n是观察数
% 'sr' : 回归量近似的子集。
% 'fic' :完全独立的条件逼近。
% 例子：'FitMethod','fic'
%% PredictMethod—用于进行预测的方法
% 'exact'：精确高斯过程回归方法。默认，如果n ≤ 10000。
% 'bcd' ：块坐标下降。默认，如果n > 10000。
% 'sd' : 数据点近似的子集。
% 'sr' : 回归量近似的子集。
% 'fic' :完全独立的条件逼近。
% 例子： 'PredictMethod','bcd'
%% 'KernelFunction' : 协方差函数的形式
% 'exponential' ：指数内核。
% 'squaredexponential' ：平方指数核。
% 'matern32' ：带参数 3/2 的母体内核。
% 'matern52' ：带参数 5/2 的母体内核。
% 'rationalquadratic' ：有理二次核。
% 'ardexponential' ：每个预测器具有单独的长度尺度的指数内核。
% 'ardsquaredexponential' ：每个预测器具有单独的长度尺度的平方指数内核。
% 'ardmatern32' ：具有参数 3/2 的母体内核和每个预测器的单独长度尺度。
% 'ardmatern52' ：具有参数 5/2 和每个预测器的单独长度尺度的母体内核。
% 'ardrationalquadratic' ：每个预测器具有单独的长度尺度的有理二次核。
% 例子 'KernelFunction','ardsquaredexponential'
%% 设置'Standardize',1
%则软件将分别按列均值和标准差对预测变量数据的每一列进行居中和缩放
 %%
load('model.mat') %训练时间较长，建议保存训练好的模型
 [y_test,~,limit] = predict(gprMdl,x); %y_test预测值，limit为上限和下限
 x_test=1:length(y_test);
 Lower=limit(:,1);
Upper=limit(:,2);
%% 计算误差
MAPE=sum(abs(y_test-y)./y)/length(y);
disp('平均绝对误差为')
disp(MAPE)
%% 计算实际值在上下限的概率
y1=y-Lower;
y2=Upper-y;
y3=(y1>0)&(y2>0);
acu=sum(y3)/length(y3);
disp('实际值在预测上下限区间的概率为')
disp(acu)
%% 画图
 h1=fill([x_test(3000:end),fliplr(x_test(3000:end))],[Lower(3000:end)',fliplr(Upper(3000:end)')],'r','DisplayName','uncertain');
hold on
COLOR=[214	237	254]/255;
COLOR1=[128 199	252]/255;
COLOR2=[190 210	254]/255;
COLOR3=[184 184	246]/255;
h1.FaceColor = COLOR;%定义区间的填充颜色      
h1.EdgeColor =[1,1,1];%边界颜色设置为白色
alpha .2   %设置透明色
plot(x_test(3000:end),y_test(3000:end),'Color',COLOR1,'LineWidth',1,'DisplayName','prediction') 
hold on
plot(x_test(3000:end),Lower(3000:end),'--','Color',COLOR2,'DisplayName','Lower  Limit')
hold on
plot(x_test(3000:end),Upper(3000:end),'--','Color',COLOR3,'DisplayName','Upper  Limit')
hold on
plot(x_test(3000:end),y(3000:end),'+','DisplayName','True Data');
legend('show','Location','Best');