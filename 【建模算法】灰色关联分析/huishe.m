%https://mp.weixin.qq.com/s/biHzf62rR1u1TWIJPCVJ9Q
clc;clear;close all;
data=[22578	27569.1	4986.5	2567.67	267.98	1.5429	1.172
25698	29483.6	5048.3	3131	348.51	1.8546	1.2514
27896	31589.2	5129.4	3858.2	429.1	2.0369	1.0254
29540	34893.5	5569.2	4417.7	541.29	2.2589	1.189
31058	36478.4	5783.2	5158.1	647.25	2.4276	1.4213
35980	38695.1	6045.1	6150.1	736.45	2.5678	1.5304
39483	40746.2	6259.2	7002.8	850	2.8546	1.7421];
%% 标准化
data1=mapminmax(data',0,1);%标准化到0.002-1区间
data1=data1';
%%
figure(1)
t=[2007:2013];
plot(t,data1(:,1),'LineWidth',2)
hold on 
for i=1:4
    plot(t,data1(:,3+i),'--')
    hold on
end
xlabel('year')
legend('x1','x4','x5','x6','x7')
title('灰色关联分析')
%% 计算 x4,x5,x6,x7 与 x1之间的灰色关联度
%得到其他列和参考列相等的绝对值
data2=abs(data1(:,4:end)-data1(:,1));
%得到绝对值矩阵的全局最大值和最小值
d_max=max(max(data2));
d_min=min(min(data2));
%灰色关联矩阵
a=0.5;   %分辨系数
data3=(d_min+a*d_max)./(data2+a*d_max);
xishu=mean(data3);
disp(' x4,x5,x6,x7 与 x1之间的灰色关联度分别为：')
disp(xishu)