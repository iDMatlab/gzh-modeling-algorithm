%https://mp.weixin.qq.com/s/biHzf62rR1u1TWIJPCVJ9Q
clc;clear;close all;
%因子分析法里面的上市公司数据
data=[220	190	180	170
6	8	8	7
30	25	28	23
10	9	7	8
10	8	7	7
5	3	4	2];%横坐标为评价指标，纵坐标为评价对象
%% 标准化
data1=mapminmax(data,0,1);%标准化到0.002-1区间
data1=data1';
V_max=max(data1);%最大参考行，指标最大
V_min=min(data1);%最小参考行，指标最小
%% 与最大值的灰色相关系数
data2=abs(data1-V_min);
%得到绝对值矩阵的全局最大值和最小值
d_max=max(max(data2));
d_min=min(min(data2));
%灰色关联矩阵
a=0.5;   %分辨系数
data3=(d_min+a*d_max)./(data2+a*d_max);
xi_min=mean(data3');
%%  与最小值的灰色相关系数
data2=abs(data1-V_max);
%得到绝对值矩阵的全局最大值和最小值
d_max=max(max(data2));
d_min=min(min(data2));
data3=(d_min+a*d_max)./(data2+a*d_max);
xi_max=mean(data3');
%% 综合评分
%与最大指标行相关系数越大，最小指标构成的行相关系数越小得分大
Score=1./(1+xi_min./xi_max).^2;
for i=1:length(Score)
    fprintf('第%d个投标者评分为：%4.2f\n',i,Score(i));   
end
