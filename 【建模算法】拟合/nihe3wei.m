%https://mp.weixin.qq.com/s/_lTMwO6owOTnmqY5Bc7zGQ
clc;clear;
data=xlsread('附件1.xlsx');
load('标签.mat')
for i=1:length(unique(la))
    A(i)={data(find(la==i),:)};
end
%% 构造x-y-z数据
x=[1:21];
y=[250;275;300;325;350;400;450];
for i=1:length(x)
    for j=1:length(y)
        B=A{1,i};
        %构造维度一样的数据
        if j<=length(B(:,1))
            Z(i,j)=B(j,2);
        else
            Z(i,j)=0;
        end
    end
end
cftool
%%
% yandata=xlsread('颜色图.xlsx');
% yandata1=yandata(1:6,:);% 取7—12行颜色
% color=yandata1/255;
color1=[0.996,0.929,0.925;0.839,0.929,0.996; 0.760,0.972,0.996;0.639,1,0.690;...
    0.501,0.780,0.988;0.686,0.913,0.270];
color2=[1,0.898,0.992;1,0.772,0.7723;1,0.756,0.592;1,0.819,1;...
    0.835,0.835,1;0.812,0.608,1];
colormap(color2)