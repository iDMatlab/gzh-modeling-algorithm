%https://mp.weixin.qq.com/s/_lTMwO6owOTnmqY5Bc7zGQ
% clc;clear;close all;
y=[143.43,562.21,583.19,934.28,1071.91,1664.45,1580.53,...
    1625.6,1199.33,391.3,432.39,37.46];
x=(1:length(y));
plot(y,'*','LineWidth',1)
xlabel('x');ylabel('y')
hold on
cftool
%% 引用参数
%将 fittedmodel 保存为fitdata1.mat
load('fitdata1.mat')%保存导出数据再导入
A=fittedmodel;
y1=A(x);
plot(y1)
hold on
legend('真实数据','拟合数据')
%% 复制参数
a=-48.02; b=621.6;c =-586.9;
y2=a*x.^2+b*x+c;y2=y2';
plot(y2)