%https://mp.weixin.qq.com/s/qGss542FdFlBIu1du3bqoQ
%公众号：好玩的MATLAB
clc;clear;close all
num=0:10:200000;
mypi=ones(1,length(num));
for j=1:length(num)
    n=num(j);m=0;
    for i=1:n
        if (-1+2*rand)^2+(-1+2*rand)^2<=1
            m=m+1;
        end
    end
    mypi(j)=4*m/n;
end
plot(mypi)
hold on
line([0 ,length(num)*1.1],[pi,pi],'color','r')
text(0,pi,'\pi','color','r','fontsize',16)
legend('模拟π','实际π')
grid minor

%% 公众号：好玩的MATLAB
clc;clear;close all;
num=0:500:10^6;
s=ones(1,length(num));
for j=1:length(num)
    n=num(j);
    a=0;b=1;
    d=max(a,b)+1;
    m=0;
    for i=1:n
        x=a+rand*(b-a);
        y=d*rand;
        if y<=x^2
            m=m+1;
        end
    end
    s(j)=m/n*d*(b-a);
end
plot(s)
hold on
line([0 ,length(num)*1.1],[1/3,1/3],'color','r')
text(0,pi,'1/3','color','r','fontsize',16)
legend('模拟','实际1/3')
grid minor