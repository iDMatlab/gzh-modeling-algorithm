%https://mp.weixin.qq.com/s/tNXQK5GbiSqvWJoyTCqmyQ
clc;clear;
data=xlsread('风场运行数据.xlsx');
%指标正向    化处理后数据为data1
data1=data;
%%负向指标（越小越优型指标）处理
index=[4,6,7];%负向指标位置
k=0.1;
for i=1:length(index)
  data1(:,index(i))=1./(k+max(abs(data(:,index(i))))+data(:,index(i)));
end
%数据标准化 
data2=data1;
for j=1:size(data1,2)
    data2(:,j)= data1(:,j)./sqrt(sum(data1(:,j).^2));
end
%计算变异系数
A=mean(data2);%求每列平均值
S=std(data2);  %求每列方差
V=S./A; %变异系数
%计算权重
w=V./sum(V);
%计算得分
s=data2*w';
Score=100*s/max(s);
for i=1:length(Score)
    %A(i,:)=[row(i), col(i), rho_1(row(i), col(i))];
    fprintf('第%d个风场百分制评分为：%d\n',i,Score(i));   
end
