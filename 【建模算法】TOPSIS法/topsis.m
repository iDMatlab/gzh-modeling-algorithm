%https://mp.weixin.qq.com/s/JRSxrSOUoCHg1OpaCDXT7g
clc;clear;
data=xlsread('投标单位各项指标和分值.xlsx');
%标准化    标准化处理后数据为data1
data1=data;
for j=1:size(data1,2)
    data1(:,j)= data(:,j)./sqrt(sum(data(:,j).^2));
end
%得到加权重后的数据
w=[0.3724, 0.1003,0.1991, 0.1991,0.0998,0.0485]; %使用求权重的方法求得
R=data1.*w;
%得到最大值和最小值距离
r_max=max(R);  %每个指标的最大值
r_min=min(R);  %每个指标的最小值
d_z = sqrt(sum([(R -repmat(r_max,size(R,1),1)).^2 ],2)) ;  %d+向量
d_f = sqrt(sum([(R -repmat(r_min,size(R,1),1)).^2 ],2)); %d-向量  
%sum(data,2)对行求和 ，sum(data）默认对列求和
%得到得分
s=d_f./(d_z+d_f );
Score=100*s/max(s);
for i=1:length(Score)
    fprintf('第%d个投标者百分制评分为：%d\n',i,Score(i));   
end
